CREATE TABLE [dbo].[Patient]
(
[Name] [nchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FName] [nchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Id] [tinyint] NOT NULL,
[DoctorId] [tinyint] NOT NULL,
[InDate] [date] NOT NULL,
[OutDate] [date] NOT NULL,
[DiseaseType] [nchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PhoneNumber] [tinyint] NOT NULL,
[NationalId] [tinyint] NOT NULL,
[Address] [nchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BirthDate] [date] NOT NULL,
[Weight] [tinyint] NOT NULL,
[Height] [tinyint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Patient] ADD CONSTRAINT [PK_Patient] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Patient] ADD CONSTRAINT [FK_Patient_Doctor] FOREIGN KEY ([DoctorId]) REFERENCES [dbo].[Doctor] ([Id])
GO
