namespace SBDomain.Model.ShirtModel
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ShirtDB : DbContext
    {
        public ShirtDB()
            : base("name=ShirtDB")
        {
        }

        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Color> Colors { get; set; }
        public virtual DbSet<Shirt> Shirts { get; set; }
        public virtual DbSet<VW_Shirt> VW_Shirt { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>()
                .HasMany(e => e.Shirts)
                .WithRequired(e => e.Category)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Color>()
                .HasMany(e => e.Shirts)
                .WithRequired(e => e.Color)
                .WillCascadeOnDelete(false);
        }
    }
}
