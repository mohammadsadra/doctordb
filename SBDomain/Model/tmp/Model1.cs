namespace SBDomain.Model.tmp
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=Model11")
        {
        }

        public virtual DbSet<VW_Doctor> VW_Doctor { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<VW_Doctor>()
                .Property(e => e.Name)
                .IsFixedLength();

            modelBuilder.Entity<VW_Doctor>()
                .Property(e => e.FName)
                .IsFixedLength();

            modelBuilder.Entity<VW_Doctor>()
                .Property(e => e.Expr2)
                .IsFixedLength();
        }
    }
}
