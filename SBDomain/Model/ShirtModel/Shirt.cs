namespace SBDomain.Model.ShirtModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Shirt")]
    public partial class Shirt
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte Id { get; set; }

        public double Price { get; set; }

        public byte CategoryId { get; set; }

        public byte ColorId { get; set; }

        public virtual Category Category { get; set; }

        public virtual Color Color { get; set; }
    }
}
