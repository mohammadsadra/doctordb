﻿/*
* Kendo UI Localization Project for v2012.3.1114
* Copyright 2012 Telerik AD. All rights reserved.
*
* Persian (fa-IR) Language Pack
*
* Project home : https://github.com/loudenvier/kendo-global
* Kendo UI home : http://kendoui.com
* Author : Bahman Nikkhahan
*
*
* This project is released to the public domain, although one must abide to the
* licensing terms set forth by Telerik to use Kendo UI, as shown bellow.
*
* Telerik's original licensing terms:
* -----------------------------------
* Kendo UI Web commercial licenses may be obtained at
* https://www.kendoui.com/purchase/license-agreement/kendo-ui-web-commercial.aspx
* If you do not own a commercial license, this file shall be governed by the
* GNU General Public License (GPL) version 3.
* For GPL requirements, please review: http://www.gnu.org/copyleft/gpl.html
*/
kendo.ui.Locale = "Persian (fa-IR)";

kendo.ui.Grid.prototype.options.messages =
    $.extend(kendo.ui.Grid.prototype.options.messages, {
        commands: {
            cancel: "لغو تغییرات",
            canceledit: "انصراف",
            create: "ایجاد رکورد جدید",
            destroy: "حذف",
            edit: "ویرایش",
            excel: "خروجی اکسل",
            pdf: "خروجی PDF",
            save: "ذخیره تغییرات",
            update: "به روزرسانی"
        },
        editable: {
            cancelDelete: "انصراف",
            confirmDelete: "حذف",
            confirmation: "آیا از حذف این رکورد مطمئن هستید؟"
        }
    });
kendo.ui.ColumnMenu.prototype.options.messages =
    $.extend(kendo.ui.ColumnMenu.prototype.options.messages, {

	    /* COLUMN MENU MESSAGES 
	   ****************************************************************************/
        sortAscending: "مرتب سازی صعودی",
        sortDescending: "مرتب سازی نزولی",
        filter: "فیلتر",
        columns: "ستون ها",
        lock: "قفل کردن",
        done: "تمام",
        unlock: "بارکردن قفل"
        /***************************************************************************/
    });

kendo.ui.Groupable.prototype.options.messages =
    $.extend(kendo.ui.Groupable.prototype.options.messages, {

        /* GRID GROUP PANEL MESSAGES 
         ****************************************************************************/
        empty: "ستون ها را جهت گروه بندی در اینجا قرار دهید"
        /***************************************************************************/
    });

kendo.ui.FilterMenu.prototype.options.messages =
    $.extend(kendo.ui.FilterMenu.prototype.options.messages, {

        /* FILTER MENU MESSAGES 
         ***************************************************************************/
        info: "نشان دادن مواردی که:",        // sets the text on top of the filter menu
        filter: "فیلتر",      // sets the text for the "Filter" button
        clear: "پاک کردن",        // sets the text for the "Clear" button
        // when filtering boolean numbers
        isTrue: "درست باشد", // sets the text for "isTrue" radio button
        isFalse: "نادرست باشد",     // sets the text for "isFalse" radio button
        //changes the text of the "And" and "Or" of the filter menu
        and: "و",
        or: "یا",
        selectValue: "-انتخاب کنید-"
        /***************************************************************************/
    });

kendo.ui.FilterMenu.prototype.options.operators =
    $.extend(kendo.ui.FilterMenu.prototype.options.operators, {

        /* FILTER MENU OPERATORS (for each supported data type) 
         ****************************************************************************/
        string: {
            eq: "برابر",
            neq: "مخالف",
            startswith: "شروع می شوند",
            contains: "دارا می باشند",
            doesnotcontain: "دارا نمی باشند",
            endswith: "خاتمه می یابند"
        },
        number: {
            eq: "مساوی",
            neq: "مخالف",
            gte: "بزرگتر یا مساوی",
            gt: "بزرگتر",
            lte: "کوچکتر یا مساوی",
            lt: "کوچکتر"
        },
        date: {
            eq: "مساوی",
            neq: "مخالف",
            gte: "بزرگتر یا مساوی",
            gt: "بزرگتر",
            lte: "کوچکتر یا مساوی",
            lt: "کوچکتر"
        },
        enums: {
            eq: "برابر",
            neq: "مخالف"
        }
        /***************************************************************************/
    });

kendo.ui.Pager.prototype.options.messages =
    $.extend(kendo.ui.Pager.prototype.options.messages, {

        /* PAGER MESSAGES 
         ****************************************************************************/
        display: "{0} - {1} از {2} مورد",
        empty: "موردی یافت نشد",
        page: "صفحه",
        of: "از {0}",
        itemsPerPage: "تعداد موارد در هر صفحه",
        first: "اولین",
        previous: "قبلی",
        next: "بعدی",
        last: "آخرین",
        refresh: "بازنشانی",
        morePages: "صفحات بیشتر"
        /***************************************************************************/
    });

kendo.ui.Validator.prototype.options.messages =
    $.extend(kendo.ui.Validator.prototype.options.messages, {

        /* VALIDATOR MESSAGES 
         ****************************************************************************/
        required: "وارد نمودن {0} الزامی است.",
        pattern: "{0} را صحیح وارد نمائید.",
        min: "{0} باید بزرگتر از {1} باشد",
        max: "{0} باید کوچکتر از {1} باشد",
        step: "{0} صحیح نمی باشد.",
        email: "{0} به عنوان آدرس ایمیل صحیح وارد نشده است.",
        url: "{0} به عنوان آدرس اینترنتی صحیح وارد نشده است",
        date: "{0} به عنوان تاریخ صحیح وارد نشده است"
        /***************************************************************************/
    });

kendo.ui.ImageBrowser.prototype.options.messages =
    $.extend(kendo.ui.ImageBrowser.prototype.options.messages, {

        /* IMAGE BROWSER MESSAGES 
         ****************************************************************************/
        uploadFile: "بارگذاری فایل",
        orderBy: "مرتب سازی با",
        orderByName: "مرتب سازی با نام",
        orderBySize: "مرتب سازی با اندازه",
        directoryNotFound: "مسیر مورد نظر یافت نشد.",
        emptyFolder: "خالی نمودن پوشه",
        deleteFile: 'آیا مطمئن هستید که "{0}" پاک شود؟',
        invalidFileType: "فایل انتخاب شده \"{0}\" نامعتبر است. فایل های پشتیبانی شده عبارتند از: {1}.",
        overwriteFile: "فایل با نام \"{0}\" در مسیر مورد نظر وجود دارد. روی آن نوشته شود؟",
        dropFilesHere: "فایل ها را اینجا قرار دهید"
        /***************************************************************************/
    });

kendo.ui.Editor.prototype.options.messages =
    $.extend(kendo.ui.Editor.prototype.options.messages, {

        /* EDITOR MESSAGES 
         ****************************************************************************/
        bold: "پررنگ",
        italic: "مورب",
        underline: "زیرخط",
        strikethrough: "strikethrough",
        superscript: "بالانویس",
        subscript: "زیرنویس",
        justifyCenter: "مرتب سازی به مرکز",
        justifyLeft: "مرتب سازی به چپ",
        justifyRight: "مرتب سازی به راست",
        justifyFull: "مرتب سازی کامل",
        insertUnorderedList: "درج لیست نامرتب",
        insertOrderedList: "درج لیست مرتب",
        indent: "افزایش فاصله",
        outdent: "کاهش فاصله",
        createLink: "ایجاد پیوند",
        unlink: "حذف پیوند",
        insertImage: "درج تصویر",
        insertHtml: "درج HTML",
        fontName: "نام قلم",
        fontNameInherit: "قلم",
        fontSize: "اندازه قلم",
        fontSizeInherit: "اندازه قلم",
        formatBlock: "قالب دهی",
        foreColor: "رنگ",
        backColor: "رنگ پس زمینه",
        style: "طرح",
        emptyFolder: "خالی نمودن مسیر",
        uploadFile: "بارگذاری فایل",
        orderBy: "مرتب سازی با:",
        orderBySize: "مرتب سازی بر اساس اندازه",
        orderByName: "مرتب سازی بر اساس نام",
        invalidFileType: "فایل انتخاب شده\"{0}\" نامعتبر است. فایل های پشتیبانی شده عبارتند از: {1}.",
        deleteFile: 'آیا مطمئن هستید که  "{0}" پاک شود؟',
        overwriteFile: "فایل با نام \"{0}\" در مسیر مورد نظر وجود دارد. روی آن نوشته شود؟",
        directoryNotFound: "مسیر مورد نظر یافت نشد",
        imageWebAddress: "آدرس اینترنتی تصویر",
        imageAltText: "متن جایگزین",
        dialogInsert: "درج",
        dialogButtonSeparator: "یا",
        dialogCancel: "انصراف"
        /***************************************************************************/
    });


kendo.ui.FileBrowser.prototype.options.messages =
    $.extend(kendo.ui.FileBrowser.prototype.options.messages, {
        uploadFile: "بارگزاری",
        orderBy: "مرتب سازی بر اساس",
        orderByName: "نام",
        orderBySize: "اندازه",
        directoryNotFound: "فولدر مورد نظر پیدا نشد",
        emptyFolder: "فولدر خالی",
        deleteFile: 'آیا از حذف "{0}" اطمینان دارید؟',
        invalidFileType: 'انتخاب فایل با پسوند "{0}" امکانپذیر نیست. پسوندهای پشیتبانی شده: {1}',
        overwriteFile: 'فایل با نام "{0}" در فولدر انتخابی وجود دارد. آیا می خواهید آن را بازنویسی کنید؟',
        dropFilesHere: "فایل را به اینجا بکشید",
        search: "جستجو"
    });


kendo.ui.FilterCell.prototype.options.messages =
    $.extend(kendo.ui.FilterCell.prototype.options.messages, {
        isTrue: "درست باشد",
        isFalse: "درست نباشد",
        filter: "فیلتر",
        clear: "پاک کردن",
        operator: "عملگر"
    })


kendo.ui.FilterCell.prototype.options.operators =
    $.extend(kendo.ui.FilterCell.prototype.options.operators, {
        string: {
            eq: "برابر باشد با",
            neq: "برابر نباشد با",
            startswith: "شروع شود با",
            contains: "شامل باشد",
            doesnotcontain: "شامل نباشد",
            endswith: "پایان یابد با"
        },
        number: {
            eq: "برابر باشد با",
            neq: "برابر نباشد با",
            gte: "برابر یا بزرگتر باشد از",
            gt: "بزرگتر باشد از",
            lte: "کمتر و یا برابر باشد با",
            lt: "کمتر باشد از"
        },
        date: {
            eq: "برابر باشد با",
            neq: "برابر نباشد با",
            gte: "بعد از یا هم زمان باشد با",
            gt: "بعد از",
            lte: "قبل از یا هم زمان باشد با",
            lt: "قبل از"
        },
        enums: {
            eq: "برابر باشد با",
            neq: "برابر نباشد با"
        }
    });


kendo.ui.FilterMenu.prototype.options.messages =
    $.extend(kendo.ui.FilterMenu.prototype.options.messages, {
        info: "ردیف هایی را نشان بده که:",
        isTrue: "درست باشد",
        isFalse: "درست نباشد",
        filter: "فیلتر",
        clear: "پاک کردن",
        and: "و",
        or: "یا",
        selectValue: "-انتخاب مقدار-",
        operator: "عملگر",
        value: "مقدار",
        cancel: "انصراف"
    });


kendo.ui.FilterMenu.prototype.options.operators =
    $.extend(!0, kendo.ui.FilterMenu.prototype.options.operators, {
        string: {
            eq: "برابر باشد با",
            neq: "برابر نباشد با",
            startswith: "شروع شود با",
            contains: "شامل باشد",
            doesnotcontain: "شامل نباشد",
            endswith: "پایان یابد با"
        },
        number: {
            eq: "برابر باشد با",
            neq: "برابر نباشد با",
            gte: "برابر یا بزرگتر باشد از",
            gt: "بزرگتر باشد از",
            lte: "کمتر و یا برابر باشد با",
            lt: "کمتر باشد از"
        },
        date: {
            eq: "برابر باشد با",
            neq: "برابر نباشد با",
            gte: "بعد از یا هم زمان باشد با",
            gt: "بعد از",
            lte: "قبل از یا هم زمان باشد با",
            lt: "قبل از"
        },
        enums: {
            eq: "برابر باشد با",
            neq: "برابر نباشد با"
        }
    });


kendo.ui.FilterMultiCheck.prototype.options.messages =
    $.extend(kendo.ui.FilterMultiCheck.prototype.options.messages, {
        checkAll: "انتخاب همه",
        clear: "پاک کردن",
        filter: "فیلتر"
    });


kendo.ui.Grid.prototype.options.messages = $.extend(kendo.ui.Grid.prototype.options.messages, {
    commands: {
        cancel: "انصراف",
        canceledit: "انصراف",
        create: "اضافه کردن ردیف جدید",
        destroy: "حذف",
        edit: "ویرایش",
        excel: "خروجی Excel",
        pdf: "خروجی PDF",
        save: "ذخیره",
        select: "انتخاب",
        update: "ذخیره"
    },
    editable: {
        cancelDelete: "انصراف",
        confirmation: "آیا از حذف این ردیف مطمئنید؟",
        confirmDelete: "حذف"
    },
    noRecords: "اطلاعاتی وجود ندارد"
});

kendo.ui.Groupable.prototype.options.messages =
    $.extend(kendo.ui.Groupable.prototype.options.messages, {
        empty: "برای گروه بندی بر اساس یک ستون، عنوان ستون را به اینجا بکشید"
    });


kendo.ui.NumericTextBox.prototype.options =
    $.extend(kendo.ui.NumericTextBox.prototype.options, {
        upArrowText: "اضافه کردن",
        downArrowText: "کم کردن"
    });

kendo.ui.Pager.prototype.options.messages =
    $.extend(kendo.ui.Pager.prototype.options.messages, {
        allPages: "همه",
        display: "ردیف {0} تا {1} از {2} ردیف",
        empty: "ردیفی برای نمایش وجود ندارد",
        page: "صفحه",
        of: "از {0}",
        itemsPerPage: "ردیف های هر صفحه",
        first: "برو به صفحه اول",
        previous: "برو به صفحه قبل",
        next: "برو به صفحه بعد",
        last: "برو به صفحه آخر",
        refresh: "بارگزاری مجدد",
        morePages: "صفحات بیشتر"
    });

kendo.ui.PivotGrid.prototype.options.messages =
    $.extend(kendo.ui.PivotGrid.prototype.options.messages, {
        measureFields: "Drop Data Fields Here",
        columnFields: "Drop Column Fields Here",
        rowFields: "Drop Rows Fields Here"
    });

kendo.ui.PivotFieldMenu.prototype.options.messages =
    $.extend(kendo.ui.PivotFieldMenu.prototype.options.messages, {
        info: "Show items with value that:",
        filterFields: "Fields Filter",
        filter: "Filter",
        include: "Include Fields...",
        title: "Fields to include",
        clear: "Clear",
        ok: "Ok",
        cancel: "Cancel",
        operators: {
            contains: "Contains",
            doesnotcontain: "Does not contain",
            startswith: "Starts with",
            endswith: "Ends with",
            eq: "Is equal to",
            neq: "Is not equal to"
        }
    });

if (kendo.ui.TreeList) {
    kendo.ui.TreeList.prototype.options.messages =
        $.extend(true, kendo.ui.TreeList.prototype.options.messages, {
            noRows: "ردیفی برای نمایش موجود نیست",
            loading: "در حال بارگزاری...",
            requestFailed: "شکست در انجام درخواست.",
            retry: "تلاش مجدد",
            commands: {
                edit: "ویرایش",
                update: "ذخیره",
                canceledit: "انصراف",
                create: "درج ردیف جدید",
                createchild: "درج گره جدید",
                destroy: "حذف",
                excel: "خروجی Excel",
                pdf: "خروجی PDF"
            }
        });
}



if (kendo.ui.RecurrenceEditor) {
    kendo.ui.RecurrenceEditor.prototype.options.messages =
        $.extend(kendo.ui.RecurrenceEditor.prototype.options.messages, {
            frequencies: {
                never: "هیچ وقت",
                hourly: "ساعتی",
                daily: "روزانه",
                weekly: "هفتگی",
                monthly: "ماهانه",
                yearly: "سالیانه"
            },
            hourly: {
                repeatEvery: "تکرار کن هر: ",
                interval: " ساعت"
            },
            daily: {
                repeatEvery: "تکرار کن هر: ",
                interval: " روز"
            },
            weekly: {
                interval: " هفته",
                repeatEvery: "تکرار کن هر: ",
                repeatOn: "تکرار کن در: "
            },
            monthly: {
                repeatEvery: "تکرار کن هر: ",
                repeatOn: "تکرار کن در: ",
                interval: " ماه",
                day: "روز "
            },
            yearly: {
                repeatEvery: "تکرار کن هر: ",
                repeatOn: "تکرار کن در: ",
                interval: " سال",
                of: " از "
            },
            end: {
                label: "پایان:",
                mobileLabel: "پایان",
                never: "هیچ وقت",
                after: "بعد از ",
                occurrence: " دفعات وقوع",
                on: "در "
            },
            offsetPositions: {
                first: "اول",
                second: "دوم",
                third: "سوم",
                fourth: "چهارم",
                last: "آخر"
            },
            weekdays: {
                day: "روز",
                weekday: "روز هفته",
                weekend: "پایان هفته"
            }
        });
}

if (kendo.ui.Scheduler) {
    kendo.ui.Scheduler.prototype.options.messages =
        $.extend(!0, kendo.ui.Scheduler.prototype.options.messages, {
            allDay: "تمام روز",
            date: "تاریخ",
            event: "رخداد",
            time: "زمان",
            showFullDay: "نمایش تمام روز",
            showWorkDay: "نمایش ساعات کاری",
            today: "امروز",
            save: "ذخیره",
            cancel: "لغو",
            destroy: "حذف",
            deleteWindowTitle: "حذف رخداد",
            ariaSlotLabel: "از " + " {0:t} " + " تا " + " {1:t}" + " انتخاب شده است ",
            ariaEventLabel: "{0} on {1:D} at {2:t}",
            editable: {
                confirmation: "آیا از حذف این رخداد مطمئن هستید?"
            },
            views: {
                day: "روزانه",
                week: "هفتگی",
                workWeek: "هفته کاری",
                agenda: "فهرست جلسات",
                month: "ماهانه",
                timeline: "خط زمانی"
            },
            recurrenceMessages: {
                deleteWindowTitle: "حذف آیتم تکرار شونده",
                deleteWindowOccurrence: "تنها رخداد کنونی حذف شود",
                deleteWindowSeries: "کل سری حذف شود",
                editWindowTitle: "ویرایش آیتم تکرار شونده",
                editWindowOccurrence: "ویرایش رخداد کنونی",
                editWindowSeries: "ویرایش سری",
                deleteRecurring: "آیا میخواهید همین رخداد را حذف کنید یا کل سری را حذف نمایید؟",
                editRecurring: "آیا میخواهید همین رخداد را ویرایش کنید یا کل سری را ویرایش نمایید؟"
            },
            editor: {
                title: "عنوان",
                start: "شروع",
                end: "پایان",
                allDayEvent: "رخداد تمام روز",
                description: "شرح",
                repeat: "تکرار",
                timezone: " ",
                startTimezone: "منطقه زمانی شروع",
                endTimezone: "منطقه زمانی پایان",
                separateTimezones: "Use separate start and end time zones",
                timezoneEditorTitle: "Timezones",
                timezoneEditorButton: "Time zone",
                timezoneTitle: "Time zones",
                noTimezone: "No timezone",
                editorTitle: "رخداد"
            }
        });
}

if (kendo.ui.SchedulerMultiLang) {    
    kendo.ui.SchedulerMultiLang.prototype.options.messages =
        $.extend(!0, kendo.ui.SchedulerMultiLang.prototype.options.messages, {
            allDay: "تمام روز",
            allEvents: "تمامی رخدادها",
            date: "تاریخ",
            event: "رخداد",
            time: "زمان",
            showFullDay: "نمایش تمام روز",
            showWorkDay: "نمایش ساعات کاری",
            today: "امروز",
            save: "ذخیره",
            cancel: "لغو",
            destroy: "حذف",
            deleteWindowTitle: "حذف رخداد",
            ariaSlotLabel: "از " + " {0:t} " + " تا " + " {1:t}" + " انتخاب شده است ",
            ariaEventLabel: "{0} on {1:D} at {2:t}",          
            views: {
                day: "روزانه",
                week: "هفتگی",
                workWeek: "هفته کاری",
                agenda: "فهرست جلسات",
                month: "ماهانه",
                timeline: "خط زمانی"
            },
            recurrenceMessages: {
                deleteWindowTitle: "Delete Recurring Item",
                deleteWindowOccurrence: "Delete current occurrence",
                deleteWindowSeries: "Delete the series",
                editWindowTitle: "Edit Recurring Item",
                editWindowOccurrence: "Edit current occurrence",
                editWindowSeries: "Edit the series",
                deleteRecurring: "Do you want to delete only this event occurrence or the whole series?",
                editRecurring: "Do you want to edit only this event occurrence or the whole series?"
            },
            editable: {
                confirmation: "آیا از حذف این رخداد مطمئن هستید?"
            },
            editor: {
                title: "عنوان",
                start: "شروع",
                end: "پایان",
                allDayEvent: "رخداد تمام روز",
                description: "شرح",
                repeat: "تکرار",
                timezone: " ",
                startTimezone: "Start timezone",
                endTimezone: "End timezone",
                separateTimezones: "Use separate start and end time zones",
                timezoneEditorTitle: "Timezones",
                timezoneEditorButton: "Time zone",
                timezoneTitle: "Time zones",
                noTimezone: "No timezone",
                editorTitle: "رخداد"
            }
        });
}

if (kendo.ui.TimelineView) {
    kendo.ui.TimelineView.prototype.options.messages =
        $.extend(!0, kendo.ui.TimelineView.prototype.options.messages, {
            defaultRowText: 'تمامی رخدادها',
            showFullDay: 'نمایش تمام روز',
            showWorkDay: 'نمایش ساعات کاری'
        });
}


if (kendo.ui.GanttMultiLang) {
    kendo.ui.GanttMultiLang.prototype.options.messages =
        $.extend(kendo.ui.GanttMultiLang.prototype.options.messages, {
            actions: {
                addChild: "اضافه کردن فرزند",
                append: "اضافه کردن کار",
                insertAfter: "اضافه کن زیر",
                insertBefore: "اضافه کن بالای",
                pdf: "گرفتن خروجی PDF"
            },
            cancel: "انصراف",
            deleteDependencyWindowTitle: "حذف رابطه",
            deleteTaskWindowTitle: "حذف کار",
            destroy: "حذف",
            editor: {
                assingButton: "ارجاع دادن",
                editorTitle: "کار",
                end: "پایان",
                percentComplete: "پیشرفت",
                resources: "منابع",
                resourcesEditorTitle: "منابع",
                resourcesHeader: "منابع",
                start: "شروع",
                title: "عنوان",
                unitsHeader: "واحدها"
            },
            save: "ذخیره",
            views: {
                day: "روز",
                end: "پایان",
                month: "ماه",
                start: "شروع",
                week: "هفته",
                year: "سال"
            }
        });
}

if (kendo.ui.Switch) {
    kendo.ui.Switch.prototype.options.messages =
        $.extend(!0, kendo.ui.Switch.prototype.options.messages, {
            checked: 'روشن',
            unchecked: 'خاموش',
           
        });
}