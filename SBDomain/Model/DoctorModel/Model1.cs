namespace SBDomain.Model.DoctorModel
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=Model1")
        {
        }

        public virtual DbSet<Doctor> Doctors { get; set; }
        public virtual DbSet<Section> Sections { get; set; }
        public virtual DbSet<VW_Doctor> VW_Doctor { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Doctor>()
                .Property(e => e.Name)
                .IsFixedLength();

            modelBuilder.Entity<Doctor>()
                .Property(e => e.FName)
                .IsFixedLength();

            modelBuilder.Entity<Doctor>()
                .Property(e => e.Address)
                .IsFixedLength();

            modelBuilder.Entity<Doctor>()
                .Property(e => e.Expert)
                .IsFixedLength();

            modelBuilder.Entity<Section>()
                .Property(e => e.Name)
                .IsFixedLength();

            modelBuilder.Entity<Section>()
                .HasMany(e => e.Doctors)
                .WithRequired(e => e.Section)
                .WillCascadeOnDelete(false);
            modelBuilder.Entity<VW_Doctor>()
                .Property(e => e.Name)
                .IsFixedLength();

            modelBuilder.Entity<VW_Doctor>()
                .Property(e => e.FName)
                .IsFixedLength();

            modelBuilder.Entity<VW_Doctor>()
                .Property(e => e.Expr2)
                .IsFixedLength();
        }
    }
}
