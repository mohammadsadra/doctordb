CREATE TABLE [dbo].[Shirt]
(
[Id] [tinyint] NOT NULL IDENTITY(1, 1),
[Price] [float] NOT NULL,
[CategoryId] [tinyint] NOT NULL,
[ColorId] [tinyint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Shirt] ADD CONSTRAINT [PK_Shirt] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Shirt] ADD CONSTRAINT [FK_Shirt_Category] FOREIGN KEY ([CategoryId]) REFERENCES [dbo].[Category] ([Id])
GO
ALTER TABLE [dbo].[Shirt] ADD CONSTRAINT [FK_Shirt_Color] FOREIGN KEY ([ColorId]) REFERENCES [dbo].[Color] ([Id])
GO
