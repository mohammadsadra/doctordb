namespace SBDomain.Model
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DoctorDB : DbContext
    {
        public DoctorDB()
            : base("name=DoctorDB1")
        {
        }

        public virtual DbSet<Doctor> Doctors { get; set; }
        public virtual DbSet<Patient> Patients { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Doctor>()
                .Property(e => e.Name)
                .IsFixedLength();

            modelBuilder.Entity<Doctor>()
                .Property(e => e.FName)
                .IsFixedLength();

            modelBuilder.Entity<Doctor>()
                .Property(e => e.Address)
                .IsFixedLength();

            //modelBuilder.Entity<Doctor>()
              //  .Property(e => e.Part)
                //.IsFixedLength();

            modelBuilder.Entity<Doctor>()
                .Property(e => e.Expert)
                .IsFixedLength();

           // modelBuilder.Entity<Doctor>()
             //   .HasMany(e => e.Patients)
                //.WithRequired(e => e.Doctor)
               // .WillCascadeOnDelete(false);

            modelBuilder.Entity<Patient>()
                .Property(e => e.Name)
                .IsFixedLength();

            modelBuilder.Entity<Patient>()
                .Property(e => e.FName)
                .IsFixedLength();

            modelBuilder.Entity<Patient>()
                .Property(e => e.DiseaseType)
                .IsFixedLength();

            modelBuilder.Entity<Patient>()
                .Property(e => e.Address)
                .IsFixedLength();
        }
    }
}
