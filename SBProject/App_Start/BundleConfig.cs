﻿using System.Web;
using System.Web.Optimization;

namespace SBProject
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/DateTimeAndMoment").Include(
                "~/Scripts/DateTimeAndMoment/moment.min.js",
                "~/Scripts/DateTimeAndMoment/moment-with-locales.js",
                "~/Scripts/DateTimeAndMoment/moment-jalaali.js",
                "~/Scripts/DateTimeAndMoment/DateTimeCommon.js",
                "~/Scripts/DateTimeAndMoment/momentOperations.js",
                 "~/Scripts/DateTimeAndMoment/moment-duration-format.js"
                  ));

            bundles.Add(new Bundle("~/bundles/kendo").Include(
              "~/Scripts/kendo/2017.2.504/kendo.all.js",
              "~/Scripts/kendo/2017.2.504/kendo.aspnetmvc.min.js",
              "~/Scripts/kendo/2017.2.504/JalaliDate.js",
               "~/Scripts/kendo/2017.2.504/kendo.switch.min.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                  "~/Scripts/bootstrap/bootstrap.js",
                  "~/Scripts/bootstrap.min.js",
                  "~/Scripts/bootstrap-hover-dropdown.min.js",
                  "~/Scripts/bootstrap/bootstrap-rtl.js"));

            bundles.Add(new ScriptBundle("~/bundles/jQueryUI").Include(
              "~/Scripts/jQueryUI/jquery-ui-1.10.0.custom.min.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/kendoCultures/Main").Include(
              "~/Scripts/Cultures/kendo.messages.js",
              "~/Scripts/Cultures/kendo.culture.js",
              "~/Scripts/Cultures/kendo.messages.en-US.js",
              "~/Scripts/Cultures/kendo.culture.en-US.js",
              "~/Scripts/Cultures/kendo.messages.fa-IR.js",
              "~/Scripts/Cultures/kendo.culture.fa-IR.js"
          //"~/Scripts/Cultures/IPMPLanguages.js",
          //"~/Scripts/Cultures/IPMPLanguages.en-US.js",
          //"~/Scripts/Cultures/IPMPLanguages.fa-IR.js"
          ));

            bundles.Add(
              new StyleBundle("~/bundles/styles/kendo")
                  .Include(
                      "~/Content/kendo/kendo.1.common.min.css"
                      , "~/Content/kendo/kendo.a.common-bootstrap.min.css"
                      , "~/Content/kendo/kendo.a.common-bootstrap.min.css"
                      , "~/Content/kendo/kendo.b.bootstrap.min.css"
                      , "~/Content/kendo/kendo.b.bootstrap.override.css"
                      , "~/Content/kendo/kendo.bootstrap.mobile.min.css"
                      , "~/Content/kendo/kendo.ipmp.override.css"
                      , "~/Content/kendo/KendoOverride.css"
                        , "~/Content/kendo/kendoSwitch.css"
                  ));

            bundles.Add(
             new StyleBundle("~/bundles/styles/bootstrapcss")
                 .Include(
                     "~/Content/BootstrapCss/bootstrap.min.css"
                     , "~/Content/BootstrapCss/bootstrapOverride.css"
                 ));

            bundles.Add(
                new StyleBundle("~/bundles/styles/rtl")
                    .Include("~/Content/BootstrapCss/bootstrap.rtl.min.css"
                        , "~/Content/kendo/Kendo.rtl.override.css"
                        , "~/Content/kendo/KendoOverride.rtl.css"
                        , "~/Content/kendo/kendo.rtl.min.css"
                        , "~/Content/rtl/checklists.css"
                    )
            );

            bundles.Add(
               new ScriptBundle("~/bundles/scripts/rtl")
                   .Include(
                       "~/Content/rtl/styleScriptParameters.js"
                   )
           );


        }
    }
}
