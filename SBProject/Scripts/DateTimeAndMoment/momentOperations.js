function getJalalidateFromDateObject(value) {

   
    if (value instanceof JalaliDate) {
        return value;
    }
    if (typeof value == 'string') {
        return getJalalidateFromString(value);
    }
    var jm = moment(value);
    return new JalaliDate(jm.jYear(), jm.jMonth(), jm.jDate());
}


function getJalalidateFromString(value) {
   
    if (value instanceof JalaliDate) {
        return value;
    }

    if (typeof value == 'string') {
        if (!value) {
            return null;
        }
        if (value.indexOf('13') == 0 || value.indexOf('14') == 0) {
            return value;
        } else
        {
            var jm = moment(value);
            return new JalaliDate(jm.jYear(), jm.jMonth(), jm.jDate());
        }

        return null;
       
    }
}

// function getMomentDateTimeStr(culture, value) {

    // if (!culture) {
        // return "";
    // }
    // if (value === null) {
        // return "";
    // }

    // if (culture == "fa-IR") {
        // return moment(value).format('jYYYY/jMM/jDD HH:mm')
    // } else {
        // return moment(value).format('YYYY/MM/DD HH:mm')
    // }

// }