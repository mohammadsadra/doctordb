namespace SBDomain.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Doctor")]
    public partial class Doctor
    {
        public byte Id { get; set; }

        public int NationalId { get; set; }

        public int PhoneNumber { get; set; }

        [Required]
        [StringLength(10)]
        public string Name { get; set; }

        [Required]
        [StringLength(10)]
        public string FName { get; set; }

        [Required]
        [StringLength(50)]
        public string Address { get; set; }

        public byte SectionId { get; set; }

        [Column(TypeName = "date")]
        public DateTime BirthDate { get; set; }

        [Required]
        [StringLength(10)]
        public string Expert { get; set; }

        public virtual Section Section { get; set; }
    }
}
