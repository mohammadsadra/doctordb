namespace SBDomain.Model.Sample
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sub")]
    public partial class Sub
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte Id { get; set; }

        public byte ParentId { get; set; }

        public double Price { get; set; }

        public virtual Main Main { get; set; }
    }
}
