﻿generalResource = {
    culture: "fa-IR",
    code: "کد",
    amount: "مبلغ",
    values: "مقادیر",
    selectProject: "انتخاب پروژه",
    field: "مشخصه",
    link: "لینک",
    organizationUnit: "واحد سازمانی",
    previousStep: "مرحله قبل",
    nextStep: "مرحله بعد",
    show: "نمایش",
    schedule: "برنامه زمانبندی",
    projectWBS: "شکست پروژه",
    dateSequences: "توالی تاریخ ها",
    jalali: "شمسی",
    georgian: "میلادی",
    daily: "روزانه",
    weekly: "هفتگی",
    monthly: "ماهانه",
    files: "فایل ها",
    downloadFile: "دریافت فایل",
    newFile: "فایل جدید",
    editFile: "تغییر فایل",
    deleteFile: "حذف فایل",
    file: "فایل",
    pleaseWaiting: "لطفا صبر کنید",
    map: "نقشه",
    text: "متن",
    value: "مقدار",
    volumn: "حجم",
    continent: "قاره",
    country: "کشور",
    province: "استان",
    county: "شهرستان",
    township: "بخش",
    villageship: "دهستان",
    village: "آبادی",
    city: "شهر",
    direction: "rtl",
    defaultAligh: "right",
    status: "وضعیت",
    number: "تعداد",
    name: "نام",
    description: "توضیح",
    description2: "شرح",
    descriptions: "توضیحات",
    minute: "دقیقه",
    hour: "ساعت",
    day: "روز",
    date: "تاریخ",
    financial: "مالی",
    total: "جمع",
    title: "عنوان",
    type: "نوع",
    addWindowTitle: "ثبت",
    editWindowTitle: "ویرایش",
    completeData: "اطلاعات را بطور کامل وارد نمایید",
    add_succeed: "آیتم با موفقیت افزوده شد",
    do_succeed: "آیتم با موفقیت انجام شد",
    edit_succeed: "ویرایش با موفقیت انجام شد",
    delete_succeed: "حذف با موفقیت انجام شد",
    errorOccured: "در انجام عملیات اشکال پیش آمده است",
    Save: "ذخیره",
    SaveAndClose: "ذخیره و بستن",
    SaveAndStartWorkFlow: "ثبت و شروع چرخه",
    Cancel: "لغو",
    ok: "تایید",
    operationSucceed: "عملیات با موفقیت انجام شد",
    operationDone: "عملیات انجام شد.",
    acction_failed: "عملیات ناموفق بود. در صورت ادامه این وضعیت با پشتیبانی سیستم تماس بگیرید",
    operation_unsuccessful: "عملیات ناموفق بود",

    projectName: "نام پروژه",
    projectCode: "کد پروژه",
    projectManagementMethod: "روش مدیریت پروژه",
    NoRecordsFound: "هیچ رکوردی یافت نشد",

    search_showing: "نمایش",
    search_of: "از",
    search_showMore: "نتایج بیشتر",

    ItemHasRelatedItems: "این آیتم دارای آیتمهای مرتبط است.",

    remove_confirmation: "آیا از حذف آیتم انتخاب شده مطمئن هستید؟",
    removeMulti_confirmation: "آیا از حذف آیتمهای انتخاب شده مطمئن هستید؟",
    removeLink_confirmation: "آیا از حذف ارتباط آیتم انتخاب شده مطمئن هستید؟",
    removeMultiLink_confirmation: "آیا از حذف ارتباط آیتمهای انتخاب شده مطمئن هستید؟",
    remove_succeed: "آیتم مورد نظر حذف شد",
    removeMulti_succeed: "آیتمهای مورد نظر حذف شدند",
    removeLink_succeed: "حذف ارتباط با موفقیت انجام شد",

    toolbar_edit: "ویرایش",
    toolbar_remove: "حذف",
    toolbar_acctionable: "آیتمها جهت اقدام",
    toolbar_viewable: "آیتمهای قابل مشاهده",
    toolbar_userCreated: "آیتمهای ایجاد شده بوسیله من",

    isNotInRange: "مقدار وارد شده در بازه مجاز قرار ندارد",
    invalidValue: "مقدار وارد شده نامعتبر است",
    noChanges: "تغییری ایجاد نشده است",

    select_emptyValue: "مقدار تهی",
    select_noChange: "بدون تغییر",
    select_none: "هیچکدام",
    selection_noItemSelected: "آیتمی انتخاب نشده است",
    selection_optionLabel: "لطفا انتخاب نمایید...",
    loading: "در حال بارگذاری...",

    project: "پروژه",

    dynamicDetailsTab: "جزئیات دیگر",
    workflowTab: "چرخه",
    PublicDocumentsAttached: "مدارک عمومی الصاق شده",

    export: "خروجی",
    exportToExcel: "خروجی به اکسل",
    exportToPDF: "خروجی به PDF",

    generalDataFormItemTitle: "عمومی",
    BadRequest: "مقادیر وارد شده صحیح نمی باشد لطفا دوباره تلاش نمایید",

    unKnown: "تعیین نشده",

    newSave: "ثبت اطلاعات جدید",
    editInfo: "ویرایش اطلاعات",
    alertDelete: "آیا اطمینان از حذف این ردیف دارید.",
    hasDependencyItem: "حذف این ردیف ، به علت وابستگی امکان پذیر نمی باشد.",
    alertDeleteHasDependent: "کل فیلدهای وابسته و آیتمهای این ردیف حذف میشود\nآیا از حذف این اطلاعات اطمینان دارید.",
    generalSettings: "تنظیمات کلی",

    gridSettingsSaved: "تنظیمات جدول با موفقیت ذخیره شد",
    gridSettingsReset: "تنظیمات جدول با موفقیت بازنشانی شد شد",

    grid_filter_contains: "دارا باشد",
    grid_filter_startsWith: "شروع شود با",
    grid_filter_endsWith: "پایان یابد با",
    grid_filter_equalsTo: "برابر باشد با",
    grid_row: "ردیف",

    grid: "جدول",
    chart: "نمودار",

    duration_min: "دقیقه",
    duration_hor: "ساعت",
    duration_day: "روز",
    duration_mon: "ماه",

    fieldDataEntryIsRequired: "وارد نمودن این فیلد الزامی است",

    do: "انجام عملیات",
    result: "نتایج عملیات",
    default: "پیش فرض",
    error: "خطا",
    Notice: "تذکر",
    Items: "گزینه ها",
    Pixel: "پیکسل",

    planProgress: "پیشرفت برنامه ای",
    actualProgress: "پیشرفت واقعی",
    weight: "وزن",
    disciplineWeight: "وزن دیسیپلین",

    //Tree operations
    addRootTitle: "افزودن سر شاخصه جدید",
    addBranchTitle: "افزودن زیر شاخه جدید",
    editBranchTitle: "افزودن زیر شاخه جدید",
    selectNodeToAddBranch: "لطفا موردی را برای افزودن زیر شاخه انتخاب کنید",

    gridSettings: "تنظیمات جدول",
    saveGridSettings: "ذخیره تنظیمات جدول",
    resetGridSettings: "بازنشانی تنظیمات جدول",

    logic_and: "و",
    logic_or: "یا",

    stakeholder: "ذینفع",
    drophere: "اینجا رها کنید"

};

organizationUnitStrs = {
    OrganizationUnitType: "نوع واحد سازمانی"
};

progressDetailsStrs = {
    grid_Title: "عنوان",
    grid_ActivityCode: "مسیرشکست",
    grid_StartProgressActual: "پیشرفت واقعی شروع بازه",
    grid_StartProgressPlan: "پیشرفت برنامه شروع بازه",
    grid_EndProgressActual: "پیشرفت واقعی انتهای بازه",
    grid_EndProgressPlan: "پیشرفت برنامه انتهای بازه",
    grid_WeightOfWhole: "وزن در پروژه",
    grid_Percent: "درصد تاثیر",
    lable_StartDate: "تاریخ شروع بازه",
    lable_EndDate: "تاریخ پایان بازه",
    lable_TotalItemCount: "تعداد کل آیتمها",
};

genderList = [
    { ID: 1, Title: "مرد" },
    { ID: 2, Title: "زن" }
];

operators_string = {
    eq: "برابر",
    neq: "مخالف",
    startswith: "شروع می شوند",
    contains: "دارا می باشند",
    doesnotcontain: "دارا نمی باشند",
    endswith: "خاتمه می یابند",
    e: "خالی باشد",
    ne: "خالی نباشد"
};
operators_number = {
    eq: "مساوی",
    neq: "مخالف",
    gte: "بزرگتر یا مساوی",
    gt: "بزرگتر",
    lte: "کوچکتر یا مساوی",
    lt: "کوچکتر",
    b: "بین دو عدد باشد",
    nb: "بین دو عدد نباشد",
    n: "بدون مقدار باشد",
    nn: "بدون مقدار نباشد"

};
operators_date = {
    eq: "مساوی",
    neq: "مخالف",
    gte: "بزرگتر یا مساوی",
    gt: "بزرگتر",
    lte: "کوچکتر یا مساوی",
    lt: "کوچکتر"
};
operators_enums = {
    eq: "برابر",
    neq: "مخالف"
};
operators_boolean = {
    istrue: "درست باشد",
    isfalse: "غلط باشد"
};
// <*dynamicField.*>
dynamicField = {
    dynamicDetailsEmpty: "فیلدی تعریف نشده است",
    EntityType: "نوع موجودیت",
    Order: "اولویت",
    ErrorMessage: "پیام خطا",

    ListTopValue: "عبارت گزینه انتخاب نشده",
    DefaultValue: "مقدار پیش فرض",

    InputFieldSize: "اندازه داده ورودی",
    MaximumCharacters: "بیشترین تعداد کاراکتر",
    IsNumeric: "عدد می باشد",
    IsRequired: "الزامی بودن",
    IsReadOnly: "فقط خواندنی",
    DependentFieldFieldNo: "تعداد سطح وابسته",
    Active: "فعال",
    DependentField1Name: "فیلد اصلی",
    DependentField2Name: "فیلد وابسته",

    NameFieldNo1: "عنوان سطح 1",
    ErrorMessageFieldNo1: "پیام خطای سطح 1",
    ListTopValueFieldNo1: "عبارت گزینه انتخاب نشده سطح 1",
    RequiredFieldNo1: "الزامی بودن سطح 1",

    NameFieldNo2: "عنوان سطح 2",
    ErrorMessageFieldNo2: "پیام خطای سطح 2",
    ListTopValueFieldNo2: "عبارت گزینه انتخاب نشده سطح2",
    RequiredFieldNo2: "الزامی بودن سطح 2",
    DependentFieldItemName: "عنوان آیتم",
    entityTypeNotSet: "نوع موجودیت مشخص نمی باشد",
    dependentInfo: "اطلاعات فیلدهای وابسته",
    dependentInfoEdit: "ویرایش فیلدهای وابسته",
    dependentItems: "آیتمهای فیلد وابسته",
    mainDependentItems: "آیتمهای فیلد اصلی",
    insertItemRequired: "درج  حداقل یک گزینه الزامی است "
};

customFieldType =
    [
        { Title: "متن ساده", ID: 1 },
        { Title: "متن چند خطی", ID: 2 },
        { Title: "لیست تک انتخابی", ID: 4 },
        { Title: "دکمه های رادیویی", ID: 6 },
        { Title: "باکس انتخابی", ID: 8 },
        { Title: "فیلدهای وابسته", ID: 10 },
        { Title: "تاریخ", ID: 13 },
    ];
depFieldNumbers =
    [
        { Title: "دو سطحی", ID: "2" }
    ];
checkBoxDefaultValue =
    [
        { Title: "انتخاب", ID: "1" },
        { Title: "عدم انتخاب", ID: "2" }
    ];

pleaseSelect = {
    selectWbsItem: "لطفا یک سطح از شکست کار را انتخاب نمایید",
    selectProjectPlan: "لطفا برنامه زمانبندی را انتخاب نمایید",
    selectProjectWBS: "لطفا شکست پروژه را انتخاب نمایید",
    select: "انتخاب کنید",
    selectOrSearch: "انتخاب و یا جستجو کنید",
    pleaseCompleteRequiredFields: "لطفا داده های اجباری را تکمیل نمایید",
    pleaseSelectMethod: "لطفا یک روش را انتخاب کنید"

};

fiscalYearListStrs = {
    window_add: "اضافه کردن سال مالی",
};

ProjectTypes = {
    DeletionProjectPossible: "به دلیل وابستگی به پروژها حذف امکانپذیر نمی باشد.",
    DeletionDependencePatternPossible: "به دلیل وابستگی به الگوی شکست کار حذف امکانپذیر نمی باشد.",
};

ContractType = {
    RemovalPossibleDependencyContracts: "به دلیل وابستگی به قراردادها حذف امکانپذیر نمی باشد.",
};

projectGroup = {
    GreaterThanZero: "مقدار وزن زمان و یا هزینه بایستی بزرگتر از صفر باشد",
    ShouldBe100: "مجموع وزن زمان و هزینه بایستی 100 باشد",
    NoRecordsFound: "هیچ رکوردی یافت نشد",
    Creation: "ایجاد",
    CreateSubcategoriesIn: "ایجاد زیر شاخه در",
    NameOfGroup: "نام گروه",
    Icon: "شمایل",
    MethodWeightingSubProjects: "روش وزندهی پروژه های زیر مجموعه",
    Handy: "دستی",
    BasedOnTime: "بر اساس زمان",
    BasedOnCost: "بر اساس هزینه",
    BasedOnTimeAndCost: "بر اساس زمان و هزینه",
    TimeWeight: "وزن زمان",
    CostWeight: "وزن هزینه",
    Equally: "به صورت مساوی",
    ProjectsHaveBeenSuccessfullyAdded: "پروژه ها با موفقیت اضافه شدند.",
    WeighingWasDone: "وزن دهی انجام شد.",
    projectName: "نام پروژه",
    ProjectWeight: "وزن پروژه",
    ProjectAutoInsert: "درج خودکار پروژه",
    InsertProject: "درج پروژه",
    TheOperationWasUnsuccessful: "عملیات ناموفق بود",
    Code: "کد",
    ContractNumber: "شماره قرارداد",
    ContractDate: "تاریخ قرارداد",
    Plan: "طرح",
    Location: "محل",
    ProjectStatus: "وضعیت پروژه",
    TypeOfProject: "نوع پروژه",
    TypeOfProjectCurrency: "نوع ارز پروژه",
    TypeOfProjectDate: "نوع تاریخ پروژه",
    ProjectManager: "راهبر پروژه",
    ProjectManagerUsername: "نام کاربری راهبر پروژه",
    Project: "پروژه",
    Select: "انتخاب کنید ...",
    SelectFilde: "... انتخاب کنید ...",
    EnteringThisFieldIsRequired: "وارد نمودن این فیلد الزامی است",
    Weight: "وزن",
    RebalancingBalancingIsDone: "بازسازی توازن انجام شود",
};

ReportHierarchy = {
    NameOfHierarchy: "نام",
    DescriptionOfHierarchy: "شرح",
    Up: "بالا (جانمایی ردیف)",
    UpInactive: "بالا (غیر فعال)",
    Down: "پایین (جانمایی ردیف)",
    DownInactive: "پایین (غیر فعال)",
    CreateTitle: "ایجاد شاخه",
    CreateSubTitle: "ایجاد زیر شاخه",
    EditTitle: "ویرایش",
};

projectSettings = {
    showLastNotApproveProgress: "نمایش آخرین پیشرفت تایید نشده",
    registerFirstProgress: "ثبت اولین پیشرفت",
    workFlowStarted: "شروع چرخه",
    registerHundredProgress: "ثبت پیشرفت 100 درصد",
    workFlowFinished: "پایان چرخه",
    dataFormItemActualStartDate: "شروع واقعی قلم کاری",
    dataFormItemActualEndDate: "پایان واقعی قلم کاری",
    canEditActualDate: "امکان ویرایش تاریخ های شروع و پایان واقعی",
    projectSettingsTitle: "تنظیمات پروژه",
    showDatePart: "نمایش قسمت تاریخ ها در شناسنامه ",
    projectDFIAccessIsDisciplineBased: "در دسترسی به اقلام کاری دیسیپلین کاربر چک شود",
};

ProgressSettings = {
    DelayReasonInputEnabled: "فعال بودن درج علل تاخیر",
    DelayReasonInputMandatoryMarginPercent: "آستانه میزان انحراف درصد پیشرفت برای اجباری بودن درج علل تاخیر",
    DelayReasonInputEnabledMarginPercent: "آستانه میزان انحراف درصد پیشرفت برای نمایش علل تاخیر",
    AllowedActualMoreThanPlanPercent: "میزان انحراف پیشرفت واقعی قابل قبول بیشتر از پیشرفت برنامه ای",
};

gdmSettings = {
    MaxFileSize: "حداکثر سایز فایل قابل قبول",
    AllowedFileExtensions: "فرمتهای فایل قابل قبول (با کاما جدا کنید، * خالی به معنای قبول همه فرمتهاست)",
    CreateMD5Checksum: "ایجاد چک سام MD5 برای فایلها",
    DenyDownloadIfChecksumNull: "جلوگیری از دانلود فایل در صورت خالی بودن چک سام",
    DenyDownloadIfChecksumDiffer: "جلوگیری از دانلود فایل در صورت متفاوت بودن چک سام",
};

dashboardStrs = {
    totalView: "نمای کلی",
    project: "پروژه",
    projectGroup: "گروه پروژه",
    widget_add_succeed: "ویجت جدید با موفقیت افزوده شد",
    widget_configSaved_succeed: "تنظیمات اختصاصی ویجت با موفقیت ذخیره شد",
    widget_settingsSaved_succeed: "تنظیمات عمومی ویجت با موفقیت ذخیره شد",
    widget_onlySupports_project: "این ویجت در حال حاضر تنها از نمایش در بخش پروژه ها پشتیبانی میکند",
    widget_onlySupports_projectGroup: "این ویجت در حال حاضر تنها از نمایش در بخش گروه پروژه ها پشتیبانی میکند",
    widget_onlySupports_projectOrProjectGroup: "این ویجت در حال حاضر تنها از نمایش در بخش پروژه ها و گروه پروژه ها پشتیبانی میکند",
    widget_onlySupports_total: "این ویجت در حال حاضر تنها از نمایش در بخش نمای کلی پشتیبانی میکند",
    widget_onlySupports_totalOrProjectGroup: "این ویجت در حال حاضر تنها از نمایش در بخش نمای کلی و یا گروه پروژه پشتیبانی میکند",
    widget_onlySupports_ProjectOrProjectGroup: "این ویجت در حال حاضر تنها از نمایش در بخش های پروژه و گروه پروژه پشتیبانی میکند",
    layout_save_succeed: "چیدمان کنونی با موفقیت ذخیره شد",
    youDoNotHaveViewPermission: "شما دسترسی مشاهده این ویجت را ندارید",

    widgetTotalSettings: "تنظیمات عمومی ویجت",
    widgetConfigs: "تنظیمات اختصاصی ویجت",
    widgetHasNoConfig: "این ویجت دارای هیچگونه تنظیماتی نمیباشد",

    layout_replaceWithDefaultConfirm: "آیا میخواهید چیدمان کنونی با چیدمان پیشفرض جایگزین گردد؟",
    layout_replacedWithDefaultInfo: "چیدمان بخش کنونی به چیدمان پیش فرض تغییر یافت. در صورتی که تمایل داشتید این چیدمان حفظ شود با کلیک گزینه ذخیره چیدمان کنونی آنرا ذخیره کنید",
    selective_windowTitle: "انتخاب تبهای انتخابی داشبورد",
    selective_addWindowTitle: "افزودن آیتم به تبهای داشبودر",
    selective_aeSelectedItem: "آیتم انتخاب شده",
    selective_itemAlreadyAdded: "",
    projectsFilterWin_title: "فیلتر پروژه ها",

    onlineUsersCount: "تعداد کاربران آنلاین",
    filtered: "فیلتر شده",

    noDefaultProjectPlan: "برنامه زمانبندی پیش فرض برای پروژه انتخاب نشده است.",
    noDefaultProjectWBS: "شکست کار پیش فرض برای پروژه انتخاب نشده است",
    noDefaultProjectWBSOrProjectPlan: "شکست  کار و یا برنامه زمانبندی پیش فرض برای پروژه انتخاب نشده است",

};

Menu = {
    AccountType: "نوع کاربری",
    Select: "انتخاب کنید ...",
    NewBranch: "ایجاد سر شاخه جدید",
    NewBranchInactive: "ایجاد سر شاخه جدید (غیر فعال)",
    NewSubfolder: "ایجاد زیر شاخه جدید",
    NewSubfolderInactive: "ایجاد زیر شاخه جدید (غیر فعال)",
    Edit: "ویرایش",
    EditDisabled: "ویرایش (غیر فعال)",
    Delete: "حذف",
    DeleteDisabled: "حذف (غیر فعال)",
    Up: "بالا (جانمایی ردیف)",
    UpInactive: "بالا (غیر فعال)",
    Down: "پایین (جانمایی ردیف)",
    DownInactive: "پایین (غیر فعال)",
    TreeManageMenus: "درختواره مدیریت منو ها",
    DefaultTitle: "عنوان پیش فرض",
    FarsiTitle: "عنوان فارسی",
    EnglishTitle: "عنوان انگلیسی",
    Address: "آدرس",
    Example: "مثال",
    Plugin: "افزونه",
    None: "هیچکدام",
    ShowForEveryone: "نمایش برای همه",
    Active: "فعال",
    ProjectIDRequired: "شناسه پروژه نیاز دارد",
    Visible: "قابل رؤیت",
    Enabled: "فعال شده",
    Selectable: "قابل انتخاب",
    OpenInNewPage: "باز شدن در صفحه جدید",
    OrganizationalRoles: "نقش ها سازمانی",
    CreateMenuAccess: "ایجاد دسترسی منو",
    CreateMenuAccessInactive: "ایجاد دسترسی منو(غیرفعال)",
    RemoveMenuAccessFromRole: "حذف دسترسی منو از نقش",
    RemoveMenuAccessFromRoleInactive: "حذف دسترسی منو از نقش(غیر فعال)",
    AllocatedOrganizationalRoles: "نقش ها سازمانی تخصیص یافته",
    SystemManager: "مدیر سیستم",
    projectManager: "راهبر پروژه",
    NormalUser: "کاربر عادی",
    TheGivenDataRowWasNotFound: "ردیف داده مورد نظر یافت نشد.",
    PleaseFillInTheRequiredValues: "لطفا مقادیر الزامی را تکمیل نمایید.",
    ProblemConnectingToServerPleaseTryAgain: "مشکل در بر قراری ارتباط با سرور ، لطفا مجدد تلاش نمایید.",
    CreateNewMenu: "ایجاد منو جدید",
    CreateSubDirectoryMenu: "ایجاد زیر شاخه منو",
    EditTheMenuInformation: "ویرایش اطلاعات منو",
    DeleteMenuBecauseSubcategory: "به دلیل وجود زیرشاخه امکان حذف این منو وجود ندارد.",
    RecordNewInformation: "ثبت اطلاعات جدید",
    Title: "عنوان"
};

workFlowManagerStrs = {
    commentIsMandatory: "ورود توضیحات برای این کار الزامی است",

    stateTypes: "State Type",
    rolesToAcceptOrReject: "مسیرها (نقشها)یی که هنوز در این مرحله میبایست تصمیم بگیرند",
    pathAccepted: "مسیرها (نقشها)یی که تایید کرده اند",
    pathRejected: "مسیرها (نقشها)یی که رد کرده اند",
    acceptsNeededToGoToNextStep: "تعداد تایید مورد نیاز برای رفتن به مرحله بعد",
    rejectsNeededToGoToPreviousStep: "تعداد رد مورد نیاز برای برگشت به مرحله قبل",

    message_noModelSelected: "مدل چرخه هنوز مشخص نشده است. لطفا مدلی را انتخاب نمایید",
    message_noAccessToWorkflowHistory: "شما دسترسی لازم برای مشاهده تاریخچه چرخه را ندارید",
    message_noAccessToWorkflowComments: "شما دسترسی لازم برای مشاهده نظرات وارد شده در چرخه را ندارید",
    message_noAccessToStateDetails: "شما دسترسی لازم برای مشاهده جزئیات وضعیت چرخه را ندارید",

    startWorkflow: "شروع چرخه",
    restartWorkflow: "ریست چرخه",

    tabs_workflowHistory: "تاریخچه چرخه",
    tabs_comments: "نظرات",
    tabs_currentStateDetails: "مشخصات مرحله کنونی",

    steps_followingStepLabel: "تایید و ارسال به مرحله(لطفا یک مرحله را مشخص کنید)",
    steps_nextStepLabel: "تایید / مرحله بعد",
    steps_previousStepLabel: "رد / مرحله قبل",
    steps_commentStepLabel: "نظرگذاری",
    dynamicParallel_saveConditions: "ذخیره مسیرهای تایید",

    eventComments: "متن نظر تایید / رد",

    grid_history_time: "زمان",
    grid_history_logType: "نوع رخداد",
    grid_history_desc: "توضیحات",
    grid_history_fromState: "وضعیت مبدا",
    grid_history_toState: "وضعیت مقصد",
    grid_history_user: "کاربر",

    grid_comments_time: "زمان",
    grid_comments_user: "کاربر عمل کننده",
    grid_comments_acceptorTitle: "عنوان کاربر",
    grid_comments_commentBody: "متن نظر",
    grid_comments_logtype: "نوع رخداد",

    steps_turnBackStepLabel: "بازگشت",
    lastApprovedProgress: "آخرین پیشرفت تایید شده",
    lastTempProgress: "آخرین پیشرفت تایید نشده",
    nameIsDuplicate: "عنوان چرخه تکراری می باشد"
};

generalAsyncFileResource = {
    select: "انتخاب فایل ...",
    remove: "حذف",
    cancel: "لغو",
    previouslyRemoved: "این آیتم قبلا حذف شده است",
    dropFilesHere: "برای آپلود شدن فایل را اینجا رها کنید",
    statusUploading: "در حال بارگذاری...",
    statusUploaded: "انجام شد",
};

generalGridResource = {
    permissions: "دسترسی ها",
    creationTime: "تاریخ ایجاد",
    creator: "ایجاد کننده",
    lastModificationTime: "تاریخ آخرین ویرایش",
    lastModifier: "آخرین ویرایش کننده",
    total: "مجموع",
};

generalWindowResource = {
    edit: "ویرایش",
    add: "ثبت آیتم جدید"
};

contractAmendmentResource = {
    window_add: "Add Contract Amendment",
    window_edit: "Edit Contract Amendment",
};

relatedGeneralDocumentManagementResource = {
    grid_title: "عنوان",
    grid_fileName: "نام فایل",
    grid_downloadLatestVersion: "دریافت آخرین بازنگری",
    grid_sendDate: "تاریخ ارسال",
    grid_receiveDate: "تاریخ دریافت",
    grid_senderGeneralDocumentNumber: "شماره ارسال",
    grid_generalDocumentNumber: "شماره مدرک",
    grid_generatorTypeTitle: "تولید کننده مدرک",
    grid_generalDocumentVersionTitle: "بازنگری",
    grid_generalDocumentTypeTitle: "نوع",
    grid_seen: "مشاهده شده",
    getLatestVersion: "دریافت آخرین نسخه",

    window_add: "ثبت سند عمومی جدید",
    window_edit: "ویرایش سند عمومی",
    toolbar_add: "الصاق سند عمومی جدید",
    toolbar_add_dis: "الصاق سند جدید غیر فعال است و یا به آن دسترسی ندارید",
    toolbar_edit: "ویرایش سند عمومی",
    toolbar_remove: "حذف سند عمومی",
    toolbar_security: "تنظیمات امنیتی",
    toolbar_security_dis: "",
    toolbar_edit_dis: "",
    toolbar_remove_dis: "",


    grid_receiver: "دریافت کننده",
    grid_sender: "ارسال کننده",
    grid_comments: "توضیحات",
    window_versionFiles: "فایلهای بازنگری",
    window_removeTitle: "حذف فایل انتخاب شده",
    window_uploadNewFile: "بارگذاری فایل جدید",
    noAccess: " شما دسترسی لازم را برای ویرایش و یا نمایش فایلهای الصاقی این آیتم ندارید.",

    error_nofileSelected: "فایلی انتخاب نشده است",
    error_fileSizeIsBigger: "سایز فایل از حداکثر مجاز بیشتر است. حداکثر مجاز سایز فایل:",
    error_fileExtensionForbidden: "فرمت فایل مجاز نیست. فرمت فایل انتخاب شده:",
    error_fileIsInfected: "فایل ارسال شده ویروسی شناسایی شده است",
    error_generalUploadError: "مشکل عمومی در بارگذاری فایل",
    error_pleaseEnterDocumentTitle: "لطفا عنوان سند را مشخص کنید",
    error_maximumAttachementCountHasBeenPassed: "تعداد مستندات الصاقی از حد مجاز فراتر رفته است",
    error_gettingDocumentGenerators: "مشکل در دریافت اطلاعات انواع تولیدکنندگان مدارک پروژه",
    error_gettingDocumentTypes: "مشکل در دریافت اطلاعات انواع مدارک پروژه",
    error_gettingDocumentVersions: "مشکل در دریافت اطلاعات نسخ مدارک پروژه",

    messages_totalAttachmentSize: "حداکثر حجم فایلهای الصاقی نمیبایست از {0} کیلو بایت بیشتر باشد",
    messages_totalAttachmentCount: "حداکثر امکان الصاق {0} فایل وجود دارد",

    agg_dfi_HasNoAccess: "شما به این بخش دسترسی ندارید. برای دسترسی به این بخش میبایست دارای دسترسی راهبر پروژه و یا مشاهده تجمیع اسناد اقلام کاری در پروژه باشید ",
};

gdmManagementStrs = {
    folderWin_addBranchTitle: "افزودن زیر شاخه جدید",
    folderWin_addRootTitle: "افزودن سرشاخه جدید",
    folderWin_editBranchTitle: "ویرایش شاخه",
    folderHasSubfolders: "این پوشه دارای زیر شاخه است. لطفا ابتدا زیر شاخه ها را حذف کنید",
    search_noParameter: "پارمتری برای جستجو مشخص نشده است",
    noFolderSelected: "لطفا موردی را برای افزودن زیر شاخه انتخاب کنید",
};

invoiceList = {
    grid_title: "عنوان",
    grid_invoiceNumber: "شماره",
    grid_projectName: "پروژه",
    grid_contractTitle: "قرارداد",
    grid_contractNumber: "شماره قرارداد",
    grid_status: "وضعیت",
    grid_subContractor: "پیمانکار",
    grid_dateIssued: "تاریخ صدور",
    grid_dateReceived: "تاریخ دریافت",
    grid_dateAccepted: "تاریخ تایید",
    grid_isTemp: "موقت/قطعی",
    grid_Temp: "موقت",
    grid_Permanently: "قطعی",
    grid_previousAccepted: "تایید شده تا کنون",
    grid_previousAccepted_Currency: "تایید شده تا کنون - چند ارزی",
    grid_amount: "مبلغ اولیه",
    grid_finalAcceptedAmount: "کارکرد این دوره",
    grid_TotalAmount: "کارکرد کل",
    grid_TotalAmount_Currency: "کارکرد کل - چند ارزی",
    grid_finalAcceptedAmount_Currency: "کارکرد این دوره - چند ارزی",
    grid_contractAmount: "مبلغ اولیه قرارداد",
    grid_contractTotalAmount: "مبلغ کل قرارداد",
    grid_valueAddedTax: "ارزش افزوده",
    grid_AcceptedAmount: "مبلغ تایید شده",
    aewindows_InsertCurrency: "درج مقادیر ارزی",

    window_add: "صورت وضعیت جدید",
    window_edit: "ویرایش",
    toolbar_add: "ثبت صورت وضعیت جدید",
    toolbar_add_dis: "ثبت صورت وضعیت غیر فعال است و یا اجازه دسترسی به آن را ندارید",
    toolbar_edit: "ویرایش",
    toolbar_edit_dis: "ویرایش صورت وضعیت غیر فعال است و یا اجازه دسترسی به آن را ندارید",
    toolbar_remove: "حذف",
    toolbar_remove_dis: "حذف صورت وضعیت غیر فعال است و یا اجازه دسترسی به آن را ندارید",

    generalTab: "اطلاعات صورت وضعیت",
    selectContractRequired: "انتخاب قرارداد الزامی است"
};

cbsStrs = {
    newCBSSubBranchWinTitle: 'ثبت CBS شاخه جدید',
    newCBSRootWinTitle: 'ثبت CBS ریشه جدید',
    editCBSWinTitle: 'ویرایش CBS',
    removeError_hasSubItems: "امکان حذف وجود ندارد" + "\n" + "آیتم انتخاب شده دارای زیر مجموعه است. قبل از حذف ابتدا زیر مجموعه ها را حذف کنید",
    removeError_hasPayments: "امکان حذف وجود ندارد" + "\n" + "آیتم انتخاب شده دارای قراردادهای مرتبط است",
    removeError_hasContracts: "امکان حذف وجود ندارد" + "\n" + "آیتم انتخاب شده دارای پرداختهای مرتبط است",
    removeError_hasInvoices: "امکان حذف وجود ندارد" + "\n" + "آیتم انتخاب شده دارای صورت وضعیتهای مرتبط است",
    removeError_hasCashflow: "امکان حذف وجود ندارد" + "\n" + "آیتم انتخاب شده دارای برنامه های جریان نقدی مرتبط است",
    title: "عنوان",
    description: "شرح",
    weight: "وزن",
    budget: "بودجه",
    chart_projectPayementsChart: "نمودار زمانی پرداختهای کل پروژه",
    chart_selectedCBSPayementsChart: "نمودار زمانی پرداختهای CBS انتخاب شده",
    chart_totalPayments: "جمع پرداختی ",
    chart_totalComulativePayments: "جمع پرداختی تجمعی ",
};
multiCurrencyCostStrs = {
    multiCurrencyCostDataEntry: "درج مقادیر ارزی",
    enteredItemsCount: "تعداد موارد وارد شده: ",
    currencyValueNotSet: 'مقدار ارز مشخص نشده است',
    currencyTypeNotSet: 'نوع ارز مشخص نشده است',
    totalBaseCurrencyValue: "مجموع بر اساس ارز پایه",
};

issueList = {
    grid_title: "عنوان",
    grid_Description: "توضیحات",
    grid_CategoriesTitle: "دسته بندی",
    grid_StatusName: "وضعیت کنونی",
    grid_AssignedUserDetailsFistLastName: "مسئول",
    grid_DueDate: "تاریخ سررسید",
    grid_IdentificationDate: "تاریخ شناسایی",
    grid_CreatedBy: "ایجاد کننده",
    grid_LastModifiedBy: "آخرین تغییر دهنده",
    grid_CreationTime: "زمان ایجاد",
    grid_LastModificationTime: "زمان آخرین تغییر",
    grid_Severity: "شدت",
    FieldName: "آیتم",
    ActionUser: "کاربر",
    FromValue: "مقدار قبلی",
    ToValue: "مقدار جدید",
    ActionDate: "تاریخ",
    ActionDescription: "توضیحات",
    IssueHistoryWindowTitle: "تاریخچه",
    NewIssueWindowTitle: "ثبت مشکل جدید در پروژه",
    EditIssueWindowTitle: "ویرایش مشکل ",
    IssueStatus_title: "عنوان",
    IssueStatus_description: "توضیحات",
};

riskList = {
    grid_title: "عنوان",
    grid_Description: "توضیحات",
    grid_CategoriesTitle: "دسته بندی",
    grid_StatusName: "وضعیت",
    grid_AssignedUserDetailsFistLastName: "مسئول",
    grid_DueDate: "تاریخ سررسید",
    grid_IdentificationDate: "تاریخ شناسایی",
    grid_CreatedBy: "ایجاد کننده",
    grid_LastModifiedBy: "آخرین تغییر دهنده",
    grid_CreationTime: "زمان ایجاد",
    grid_LastModificationTime: "زمان آخرین تغییر",
    grid_Severity: "شدت",
    grid_type: "نوع ریسک",
    grid_probability: "احتمال",
    FieldName: "آیتم",
    ActionUser: "کاربر",
    FromValue: "مقدار قبلی",
    ToValue: "مقدار جدید",
    ActionDate: "تاریخ",
    ActionDescription: "توضیحات",
    positive: "مثبت",
    negative: "منفی",
    history: "تاریخچه",
    addOrEditRisk: "ثبت و ویرایش ریسک",
};

numberingFormats = {
    part_toolbar_add: "بخش جدید",
    part_window_add: "جزئیات بخش",
    window_add: "جزئیات فرمت",
    valueIsNotValid: "مقدار نا معنبر می باشد",
    formatParts: "بخش های فرمت",
    editSeperator: "تغییر جدا کننده",
    parts: "بخش ها",
    formatExistsForField: "برای مشخصه، قبلا فرمت شماره گذاری ایجاد شده است"

};

ProjectStatusStrs = {
    ProjectIsInThisStatus: "پروژه در این وضعیت",
    Deactivated: "غیر فعال است",
    Activated: " فعال است",
};

workSpace = {
    noAccess: "شما دسترسی لازم برای فضای کار این پروژه را ندارید",
    noDefaultProjectPlan: "برنامه زمانبندی پیش فرض برای پروژه انتخاب نشده است.",
    noDefaultProjectWBS: "شکست پیش فرض برای پروژه انتخاب نشده است",
    assginDFIToWbsConfirm: "قلم کاری آزاد انتخاب شده به شکست انتخاب شده تخصیص یابد؟ شکست انتخاب شده: ",
    assginMultiDFIToWbsConfirm: "اقلام کاری آزاد انتخاب شده به شکست انتخاب شده تخصیص یابند؟ شکست انتخاب شده: ",

    wbsToolbar_add: "افزودن زیر شاخه",
    wbsToolbar_addRoot: "افزودن ریشه",
    wbsToolbar_edit: "ویرایش",
    wbsToolbar_remove: "حذف",
    wbsToolbar_resourceUsage: "مصرف منابع",
    wbsToolbar_resourceAssign: "تخصیص منابع",

    wbsToolbar_add_dis: "افزودن زیر شاخه (غیر فعال)",
    wbsToolbar_addRoot_dis: "افزودن ریشه(خیر فعال)",
    wbsToolbar_edit_dis: "ویرایش (غیر فعال)",
    wbsToolbar_remove_dis: "حذف (غیر فعال)",
    wbsToolbar_resourceUsage_dis: "مصرف منابع (غیر فعال)",
    wbsToolbar_resourceAssign_dis: "تخصیص منابع (غیر فعال)",

    dfiToolbar_add: "افزودن",
    dfiToolbar_addMenu: "",
    dfiToolbar_edit: "ویرایش",
    dfiToolbar_remove: "حذف",
    dfiToolbar_add_dis: "افزودن (غیر فعال)",
    dfiToolbar_addMenu_dis: "",
    dfiToolbar_edit_dis: "حذف (غیر فعال)",
    dfiToolbar_remove_dis: "حذف (غیر فعال)",

    dfiToolbar_removeLink_dis: "حذف ارتباط غیر فعال است یا به آن دسترسی ندارید",
    dfiToolbar_removeLink: "حذف ارتباط",

    dfi_noProgressEditPermission: "شما دسترسی لازم برای ویرایش پیشرفت این قلم کاری را ندارید",

    dfi_activityCode: "کد فعالیت",
    dfi_actualEndDate: "تاریخ واقعی پایان",
    dfi_actualStartDate: "تاریخ واقعی شروع",
    dfi_detailsTab: "مشخصات تخصصی قلم کاری",
    dfi_discipline: "دیسیپلین",
    dfi_duration: "مدت زمان قلم کاری",
    dfi_financialProgress: "پیشرفت مالی",
    dfi_forecastEndDate: "تاریخ پیش بینی پایان	",
    dfi_generalNoEditPermission: "شما دسترسی لازم را برای ویرایش این قلم کاری ندارید",
    dfi_generalTab: "مشخصات کلی",
    dfi_generalTitleRow: "مشخصات قلم کاری",
    dfi_invoiceProgress: "پیشرفت صورت وضعیت",
    dfi_otherDetailsTab: "جزئیات دیگر",
    dfi_physicalProgress: "پیشرفت فیزیكی",
    dfi_planEndDate: "تاریخ پایان برنامه ریزی شده	",
    dfi_planManHour: "نفرساعت برنامه ریزی شده",
    dfi_planningDetailsTab: "برنامه ریزی کلی",
    dfi_planningStepsTab: "زمانبندی مراحل",
    dfi_planStartDate: "تاریخ شروع برنامه ریزی شده",
    dfi_planTitleRow: "برنامه زمانبندی قلم کاری",
    dfi_progressChart: "نمودار پیشرفت",
    dfi_progressCheckDate: "تاریخ پیشرفت",
    dfi_progressPercent: "درصد",
    dfi_progressPlan: "روش محاسبه پیشرفت برنامه ای",
    dfi_progressPlanProcedure: "روش محاسبه پیشرفت برنامه ای",
    dfi_progressPlanProcedureWarn: "در صورت عدم انتخاب روش محاسبه، پیشرفت به صورت خطی زمانی محاسبه میشود",
    dfi_progressTab: "پیشرفت	",
    dfi_progressTable: "جدول پیشرفت	",
    dfi_relatedDocsTab: "مدارک الصاق شده",
    dfi_title: "عنوان قلم کاری",
    dfi_updateProgress: "به روز رسانی",
    dfi_volumnOfWork: "کل حجم پیش بینی شده",
    dfi_volumnOfWorkUnit: "واحد حجم کار قلم کاری",
    dfi_weightOfActivity: "درصد وزن قلم کاری نسبت به فعالیت",
    dfi_weightOfWhole: "درصد وزن قلم کاری از کل پروژه",
    dfi_wFModel: "چرخه قلم کاری",
    dfi_workflow: "چرخه قلم کاری",
    dfi_workflowTab: "چرخه",
    dfi_workflowTitle: "چرخه",
    dfi_delayReason: "دلیل تاخیر",
    dfi_delayReasonComments: "شرح دلیل تاخیر",
    dfi_progressInputTab: "ورود اطلاعات",

    autoAdd_winTitle_all_SelectedWBS: "ایجاد خودکار اقلام کاری برای تمامی فعالیتهای ذیل شکست انتخاب شده",
    autoAdd_winTitle_emptyActivities_SelectedWBS: "ایجاد خودکار اقلام کاری برای فعالیتهای بدون قلم کاری ذیل شکست انتخاب شده",
    autoAdd_winTitle_all_Project: "ایجاد خودکار اقلام کاری برای تمامی فعالیتهای پروژه",
    autoAdd_winTitle_emptyActivities_Project: "ایجاد خودکار اقلام کاری برای تمامی فعالیتهای بدون قلم کاری پروژه",

    autoAdd_PleaseSelectExcel: "لطفا فایل با فرمت اکسل انتخاب نمائید",
    autoAdd_selectFile: "انتخاب فایل",
    autoAdd_create: "ایجاد",
    autoAdd_createOneDFIForEachActivity: "ایجاد قلم کاری به ازای هر فعالیت",
    autoAdd_createDFIByExcel: "ایجاد قلم کاری بر اساس فایل اکسل",

    wbs_ewinTitle: "ویرایش شکست کار",
    wbs_awinTitle: "ایجاد شکست کار جدید",
    dfi_ewinTitle: "ویرایش قلم کاری",
    dfi_awinTitle: "ایجاد قلم کاری",
    multiDfi_winTitle: "ویرایش گروهی اقلام کاری",

    wbsToolbar_search: "جستجو در کل شکست کار",
    wbsToolbar_settings: "تنظیمات",
    wbsToolbar_gantt: "گانت چارت پروژه",
    totalSettingWin_title: "تنظیمات کلی فضای کار",
    search_pleaseEnterAtLeastThreeWords: "لطفا حداقل 3 حرف برای جستجو وارد کنید",
    showType_wbs: "بر اساس شکست",
    showType_free: "اقلام کاری آزاد",
    showType_project: "تمام اقلام کاری پروژه",
    showType_mine: "اقلام کاری من",

    setAllSubDFIParamsWin_title: "بروزرسانی گروهی اقلام کاری زیر مجموعه"
};

articleResources = {
    image: "تصویر",
    CanView: "نمایش در سایت",
    active: "فعال",
    Chosen: "نمایش در صفحه اول",
    date: "تاریخ",
    title: "عنوان",
    language: "زبان",
    persian: "فارسی",
    abstract: "چکیده",
    body: "متن",
    save: "ذخیره",
    cancel: "لغو",
    preview: "پیش نمایش",
    select: "انتخاب تصویر",
    newSave: "ثبت اطلاعات جدید",
    editInfo: "ویرایش اطلاعات",
    alertDelete: "آیا اطمینان از حذف این ردیف دارید.",
    alertSuccessDelete: "حذف با موفقیت انجام شد",
    alertImageType: "لطفا عکس با فرمت مناسب بارگذاری نمایید",
};

ManageProjectProgressCalculationResources = {
    newSave: "ثبت اطلاعات جدید",
    alertDelete: "آیا اطمینان از حذف این ردیف دارید.",
    noItemSelected: "آیتمی انتخاب نشده است.",
    notValid: "مقدار وارد شده معتبر نمی باشد.",
    title: "عنوان",
    amountTimeProgress: "میزان پیشرفت زمانی",
    amountInformationProgress: "میزان پیشرفت قلم کاری",
    stageTitle: "عنوان مرحله",
    timeProgress: "پیشرفت زمانی",
    value: "مقدار از 0 تا 100",
    intelligenceProgress: "پیشرفت اطلاعاتی",
    save: "ذخیره",
    cancel: "لغو",
    alertPlannedProgress: "لطفا مراحل پیشرفت برنامه ریزی شده را ثبت نمایید.",
    function: "نوع تابع توزیع",
    creator: "ایجاد کننده",
    creationTime: "زمان ایجاد",
    lastEditedBy: "آخرین ویرایش کننده",
    lastEditedTime: "زمان آخرین ویرایش",
    editInfo: "ویرایش اطلاعات",
    alertSuccessDelete: "حذف با موفقیت انجام شد.",
    titleNotValid: "عنوان وارد شده معتبر نمی باشد.",
    plannedNotValid: "مراحل پیشرفت برنامه ریزی شده معتبر نمی باشد.",
    problemsStorageProcess: "بروز اشکال در فرآیند ذخیره سازی ، لطفا مجدد تلاش نمایید.",
    linear: "خطی",
    stepping: "پله ای",
    description: "توضیحات",
    duration: "مدت زمان مجاز انجام فعالیت"
};

wbsManagementResources = {
    grid_Name: "عنوان",
    grid_Duration: "مدت",
    grid_Code: "کد",
    grid_Weight: "وزن",
    grid_PlanStartDate: "شروع ب",
    grid_PlanEndDate: "پایان ب",
    grid_ProgressPlan: "پیشرفت ب",
    grid_ProgressActual: "پیشرفت و",
    grid_ActualStartDate: "شروع و",
    grid_ActualEndDate: "پایان و",
    grid_ActivityCode: "شناسه فعالیت",
    grid_noRecord: "رکوردی برای نمایش وجود ندارد (احتمال عدم تطابق برنامه زمانبندی و شکست انتخاب شده)",
    grid_RootTitlePath: "مسیر ریشه",
    grid_WeightPlanProgress: "پیشرفت ب وزنی",
    grid_WeightActualProgress: "پیشرفت و وزنی",
    cannotConvertToActivity: "به دلیل داشتن زیر مجموعه، امکان فعالیت بودن وجود ندارد",
    cannotConvertToNonActivity: "به دلیل داشتن اقلام کاری، امکان خروج از حالت فعالیت وجود ندارد"
};

dfiStrs = {
    title: "عنوان",
    wbsPath: "مسیر شکست کار",
    ActivityCode: "کد فعالیت",
    DisciplineName: "دیسیپلین",
    CurrentState: "وضعیت کنونی",
    WorkflowModelName: "نام چرخه",
    ProjectName: "عنوان پروژه",
    PlanStartDate: "تاریخ شروع ب",
    PlanEndDate: "تاریخ اتمام ب",
    ActivityName: "عنوان فعالیت",
    autoRelate: "تخصیص خودکار اقلام کاری"
};

dfiListResources = {
    grid_Title: "عنوان",
    grid_DisciplineName: "دیسیپلین",
    grid_ActualProgress: "پیشرفت و",
    grid_PlanProgress: "پیشرفت ب",
    grid_ActivityCode: "شناسه فعالیت",
    grid_WBSCode: "شناسه شکست",
    grid_ExtensionName: "نوع قلم کاری",
    grid_WorkflowModelName: "نام چرخه",
    grid_CurrentState: "وضعیت کنونی",
    grid_ProjectDataFormItemProgressPlanName: "برنامه پیشرفت",
    grid_PlanStartDate: "شروع برنامه ای",
    grid_PlanEndDate: "پایان برنامه ای",
    grid_ForecastEndDate: "پایان پیش بینی",
    grid_ActualStartDate: "شروع و",
    grid_ActualEndDate: "پایان و",
    grid_WeightOfWhole: " وزن از کل",
    grid_WeightOfActivity: " وزن از فعالیت",
    grid_PlanManHour: "نفرساعت برنامه ای",
    grid_ProjectName: "پروژه",
};

aeSingleDfi = {
    progressStepsGrid_stepName: "نام مرحله",
    progressStepsGrid_planDate: "تاریخ برنامه ریزی",
    progressStepsGrid_actualDate: "تاریخ واقعی",
    progressStepsGrid_isAutomatic: "خودکار",
    progressStepsGrid_isTemp: "موقت",
    progressStepsGrid_isAccepted: "تایید شده",

    progressActualGrid_checkDate: "تاریخ پیشرفت",
    progressActualGrid_creationTime: "تاریخ ثبت",
    progressActualGrid_PhysicalProgress: "پیشرفت فیزیکی",
    progressActualGrid_FinancialProgress: "پیشرفت مالی",
    progressActualGrid_InvoiceProgress: "پیشرفت صورت وضعیت",
    progressActualGrid_Creator: "ایجاد کننده",

    progressActual_From: "حداقل",
    progressActual_To: "حداکثر",

    progressActual_ProgressSuccessFullySaved: "پیشرفت با موفقیت ذخیره شد",
    progressActual_ProgressTemporarySavedAndWaitForAccept: "پیشرفت موقتا ذخیره شد و در صورت تایید اعمال خواهد شد",
    progressActual_ProgressIsMoreThanTheAllowedMarginFromPlan: "پیشرفت وارد شده از بازه مجاز بیشتر بودن از برنامه بالاتر است",
    progressActual_DelayReasonSpecificationIsMandatoryInThisVariance: "ورود علت تاخیر در انحراف از برنامه وارد شده اجباری است",
};

eMultiDfi = {
    notSameProject: "موارد انتخاب شده میبایست از یک پروژه باشند",
    notSameType: "نوع اقلام کاری انتخاب شده یکسان نیست",
    progressNotInSameState: "وضعیت پیشرفت اقلام کاری انتخاب شده یکسان نیست",
    someHasStartedWorkflows: "امکان تغییر مدل چرخه وجود ندارد زیرا که حداقل یکی از اقلام کاری انتخاب شده دارای چرخه شروع شده هستند"
};

StakeholdersStrs = {
    title: "نام شرکت",
    grid_name: "نام",
    grid_country: "نام کشور",
    window_add: "درج شرکت",
    window_edit: "ویرایش شرکت",
    StakeholderProfile: "پروفایل پیمانکار",
    itemsSelected: "شرکت/فرد انتخاب شده اند",
    nationalNo: "شماره ملی",
    nationalID: "شناسه ملی",
    nationalCode: "کد ملی",
    registerNo: "شماره ثبت",
    registerDate: "تاریخ ثبت",
    registerType: "نوع ثبت",
    registerLocation: "محل ثبت",
    Address: "آدرس",
    PostalCode: "کد پستی",
    CEOName: "نام مدیرعامل",
    Email: "پست الکترونیک",
    CallNo: "شماره تماس",
    Fax: "فکس",
    fieldofactivity: "زمینه فعالیت",
    FieldParentCaption: "ذینفع متبوع",
    IDNo: "شماره شناسنامه",
    gender: "جنسیت",
    Birthdate: "تاریخ تولد",
    grid_LegalEntityTypeName: "ماهیت قانونی",
    grid_ParentName: "ذینفع متبوع",
    juridicallegal: "حقوقی",
    naturallegal: "حقیقی",
    legalEntityTypeIsrequired: "انتخاب ماهیت اعتباری الزامی است.",
    DetailHeaderTitle: "اطلاعات {0}",
    UneditableOrganizationUnit: "امکان ویرایش واحد سازمانی در این فرم نیست. لطفا از بخش واحد سازمانی اقدام نمایید"
};
LegalEntityType = [
    { ID: 0, Name: "نامعلوم", LegalType: "" },
    { ID: 1, Name: "حقوقی", LegalType: "Company" },
    { ID: 2, Name: "حقیقی", LegalType: "Person" },
    { ID: 3, Name: "سازمانی", LegalType: "OrganizationUnit" },
];

users = {
    grid_firstName: "نام",
    grid_lastName: "نام خانوادگی",
    grid_userName: "نام کاربری",
    itemsSelected: "کاربر انتخاب شده اند",
    selectImage: "انتخاب تصویر",
    inputPassNotEqual: "پسوردهای وارد شده برابر نیستند",
    passIsEmpty: "پسوردها وارد نشده",
    selectdUserDigSignPass: "گذرواژه امضای دیجیتال کاربر انتخاب شده",
    areYouSureToSync: "از همزمانی کاربران با اکتیو دایرکتوری اطمینان دارید؟"
};

Extension = {
    grid_ExtendedControl_Name: "نام",
    grid_Extension_Name: "نام ماژول",
    grid_Description: "شرح",
    grid_IsReport: "گزارش"
};

contractList = {
    grid_ContractNumber: "شماره قرارداد",
    grid_Title: "عنوان قرارداد",
    grid_SubContractorName: "طرف قرارداد",
    grid_ProjectName: "پروژه",
    grid_ContractDate: "تاریخ قرارداد",
    grid_ContractAmount: "رقم قرارداد",
    grid_ContractCurrencyAmount: "ارقام ارزی قرارداد",
    grid_ContractTotalCurrencyAmount: "ارقام ارزی کل قرارداد",
    grid_ContractPredictedFinalAmount: "رقم پیش بینی اتمام قرارداد",
    grid_ContractPayedAmount: "مبلغ پرداخت شده تا کنون",
    grid_Name: "عنوان قرارداد",
    grid_CBSPath: "CBS",
    grid_ContractTypeName: "نوع قرارداد",
    grid_AmendedContractDate: "تاریخ تمدید بر اساس الحاقیه",
    grid_AmendmentCount: "تعداد الحاقیه",
    grid_ContractTotalAmount: "مبلغ کل قرارداد",
    grid_ContractFinishDate: "تاریخ اتمام قرارداد",
    grid_ContractFinalAmount: "مبلغ اتمام قرارداد",
    grid_ContractedStartDate: "تاریخ شروع قرارداد",
    grid_ContractAwardDate: "تاریخ ابلاغ قرارداد",
    grid_ForecastedFinishDate: "تاریخ پیش بینی اتمام",
    grid_ContractStatusName: "وضعیت قرارداد",
    grid_AwardWorkflowStatus: "وضعیت ابلاغ قرارداد",
    grid_InvoiceCount: "تعداد صورت وضعیت",
    grid_ContractFinancialTypeName: "نوع مالی قرارداد",
    grid_awardWorkflow: "چرخه ابلاغ قرارداد",

    error_hasInvoices:
        "این قرارداد دارای صورت وضعیت ثبت شده است. قبل از حذف میباست صورت وضعیتها حذف گردند",
    error_isDefaultContractForAProject:
        "این آیتم، قرارداد پیشفرض پروژه است. برای حذف آن قرارداد دیگری را در بخش تنظیمات پیشفرض پروژه قرار دهید",

    removeCBSTitle: "حذف ارتباط با CBS",

    grid_Taskmaster: "کارفرما",
    grid_Supervisor: "ناظر",
    grid_Consultant: "مشاور",

    addWindowTitle: "ثبت قرارداد جدید",
    editWindowTitle: "ویرایش قرارداد",
    errorInGettingSettings: "اشکال در دریافت تنظیمات ",
    pleaseCheckTheOrderOfSteps: "لطفا توالی مراحل را رعایت کنید",

    contractPeriodicReportSettings: "تنظیمات فرآیند نظارت و تایید پیشرفت قرارداد",
    contractProjectDFIRelation: 'جزئیات ارتباط قرارداد با پروژه',
    awardWorkflowStarted: "چرخه ابلاغ شروع شد.",
    awardWorkflowRestarted: "چرخه ابلاغ بازیابی شد.",

    aeContract_contactInfoTab: "اطلاعات قرارداد",
    aeContract_relatedProjectsTab: "پروژه های قرارداد",
    aeContract_dynamicFieldsTab: "جزئیات دیگر",
    aeContract_awardWorkflowTab: "چرخه ابلاغ",
    aeContract_relatedGeneralDocumentsTab: "مدارک الصاق شده",
    aeContract_awardWorkflow: "چرخه ابلاغ",
    aeContract_Description: "شرح قرارداد",
    aeContract_Duration: "مدت زمان قرارداد",
    aeContract_Day: "روز",
    aeContract_FinanceSources: "محل تامین اعتبار",
    aeContract_weigthSumShouldBe100: "مجموع وزن پروژه ها میبایست 100 باشد",
    aeContract_requiredDynamicFieldsAreNotFilled: "فیلد های داینامیک بصورت کامل تکمیل نشده",
    aeContract_StartDateCanNotBeAfterEndDate: "تاریخ پایان قرارداد نباید قبل از تاریخ شروع قرارداد باشد",
    aeContract_acceptInvoiceByWF: "تایید صورت وضعیت بوسیله چرخه",
    aeContract_confirmRemoveAcceptInvoiceByWF: "در صورت غیر فعال کردن تایید توسط چرخه، تمام چرخه های صورت وضعیت های قرارداد حذف خواهند شد. آیا از انجام این کار اطمینان دارید؟",
    agg_contract_HasNoAccess: 'برای مشاهده این بخش میبایست دارای دسترسی مدیریت در پروژه و یا دسترسی مدیریت قراردادها باشید',
};

amendmentListStrs = {
    addWindowTite: "ثبت الحاقیه جدید",
    editWindowTitle: "ویرایش الحاقیه",
    aeAmendment_requiredDynamicFieldsAreNotFilled: "فیلد های داینامیک بصورت کامل تکمیل نشده",
};

paymentListStrs = {
    grid_DocumentNumber: "شماره سند",
    grid_PaiementDate: "تاریخ پرداخت",
    grid_Amount: "مبلغ کل",
    grid_CostCenter: "مرکز هزینه",
    grid_PaymentBehalf: "بابت",
    grid_CreatedBy: "ایجاد کننده",
    grid_LastModifiedBy: "آخرین تغییر دهنده",
    grid_CreationTime: "زمان ایجاد",
    grid_LastModificationTime: "زمان آخرین ویرایش",
    grid_Subcontractor: "مجری پروژه",
    grid_ContractTitle: "عنوان قرارداد",
    grid_ProcurementName: "عنوان پرداخت",
    grid_ContractNumber: "شماره قرارداد",
    grid_InvoiceNumber: "شماره صورت وضعیت",
    grid_CurrencyAbbriviation: "ارز",
    generalTab: "اطلاعات کلی",
    relatedDocsTab: "مدارک الصاق شده",
    ae_project: "پروژه",
    ae_invoice: "شماره صورت وضعیت",
    ae_documentNumber: "شماره سند",
    ae_amount: "مبلغ",
    ae_paymentDate: "تاریخ پرداخت",
    ae_cbs: "CBS",
    ae_costCenter: "مرکز هزینه",
    ae_paymentBehalf: "بابت",
    ae_currencyType: "نوع ارز",
    ae_paymentDescription: "شرح پرداخت",
    ae_paymentRemarks: "توضیحات پرداخت",
    ae_discipline: "دیسیپلین"
};

amendmentManagment = {
    grid_AmendmentNumber: "شماره الحاقیه",
    grid_TotalAmount: "مبلغ کل",
    grid_AmendmentDate: "تاریخ الحاقیه",
};

ganttStrs = {
    grid_title: "عنوان",
    grid_startTime: "شروع",
    grid_endTime: "پایان",
    grid_Resources: "منابع تخصیص یافته",

    //Messages

    save: "به روز رسانی",
    start: "شروع",
    end: "پایان",

    //Actions
    addChild: "زیرشاخه جدید",
    append: "شکست جدید",
    insertAfter: "ایجاد شکست در زیر",
    insertBefore: "ایجاد شکست در بالا",
    cancel: "لغو",
    deleteDependencyConfirmation: "ارتباط حذف گردد?",
    deleteDependencyWindowTitle: "تایید حذف ارتباط",
    deleteTaskConfirmation: "شکست حذف شود?",
    deleteTaskWindowTitle: "تایید حذف شکست",
    destroy: "حذف",
    pdf: "خروجی به PDF",


    //Editor
    ed_assignButton: "منابع تخصیص یافته",
    ed_editorTitle: "ویرایش فعالیت",
    ed_end: "پایان",
    ed_percentComplete: "پیشرفت",
    ed_resources: "منابع",
    ed_resourcesEditorTitle: "تخصیص منابع فعالیت",
    ed_resourcesHeader: "منابع در دسترس",
    ed_start: "شروع",
    ed_title: "عنوان فعالیت",
    ed_unitsHeader: "واحد",

    //Views
    vw_day: "نمایش روزانه",
    vw_end: "Task End",
    vw_month: "نمایش ماهانه",
    vw_start: "Task Start",
    vw_week: "نمایش هفتگی",
    vw_year: "نمایش سالانه",
};

timeSheets = {
    toolbar_add: "کار جدید",
    toolbar_edit: "ویرایش کار",
    toolbar_remove: "حذف کار",
    startTime: "زمان شروع",
    endTime: "زمان پایان",
    totalTime: "کل زمان",
    startTask: "شروع کار",
    stopTask: "اتمام کار",
    taskRowDeleteConfirm: "از حذف کار اطمینان دارید؟",
    todayWorkStartTime: "زمان شروع کار امروز",
    todayWorkEndTime: "زمان پایان کار امروز",
    usefulTime: "زمان مفید",
    timeIsIncorrect: "زمان وارد شده صحیح نمی باشد",
    entranceTime: "زمان ورود",
    exitTime: "زمان خروج",
    myTimeSheetReport: "گزارش وضعیت کارکرد",
    pleaseSelectDate: "انتخاب تاریخ",
    timeSpent: "مدت زمان حضور",
    workStartTime: "زمان شروع کار",
    workEndTime: "زمان پایان کار",
    workDuration: "مدت زمان انجام کار",
    worksTime: "مدت زمان کارها",
    helpfulTime: "زمان مفید",
    withoutProject: "بدون پروژه",
    year: 'سال',
    month: 'ماه',
    yearly: 'سالانه',
    monthly: 'ماهانه',
    monthPerformance: 'عملکرد ماه',
    monthTotalTimeGroupByProject: 'زمان ماه به تفکیک پروژه',
    monthTotalTimeGroupByProject_showBy: 'نوع نمایش',
    monthTotalTimeGroupByProject_showBy_profitable: 'زمان مفید',
    monthTotalTimeGroupByProject_showBy_total: 'مجموع',
    date: 'تاریخ',
    weekDay: 'روز هفته',
    dayActivitiesDetails: 'جزئیات فعالیتهای روزانه',
    messages_userNotRelatedToACompany: "برای کاربر شرکت تنظیم نشده است و امکان پیدا کردن تقویم پایه برای محاسبات وجود ندارد",
    messages_userCompanyNotHaveCalendar: "برای شرکت مربوط به کاربر، تقویم پیش فرض تنظیم نشده است",
    totalDayWorkingTime: "کل زمان کاری روز",
    arrivalDelay: "تاخیر در ورود",
    hurryInExit: "تعجیل در خروج",
    extraWorkBeforeWorkStart: "اضافه کاری قبل از زمان",
    extraWorkAfterWorkStart: "اضافه کار بعد از زمان",
    userTotalWorkTime: "کل زمان کاری فرد",
    userTotalUsefulTime: "کل زمان مفید فرد",
    userTotalWorkInWorkTime: "کل زمان کاری فرد در ساعات اداری",
    userName: "نام کاربری",
    firstName: "نام",
    lastName: "نام خانوادگی",
    firstTimesheetResponsible: "مسئول اول تایم شیت",
    secondTimesheetResponsible: "مسئول دوم تایم شیت",
    thirdTimesheetResponsible: "مسئول سوم تایم شیت",
    fourthTimesheetResponsible: "مسئول چهارم تایم شیت",
    fifthTimesheetResponsible: "مسئول پنجم تایم شیت",
    firstVacationResponsible: "مسئول اول مرخصی",
    secondVacationResponsible: "مسئول دوم مرخصی",
    thirdVacationResponsible: "مسئول سوم مرخصی",
    fourthVacationResponsible: "مسئول چهارم مرخصی",
    fifthVacationResponsible: "مسئول پنجم مرخصی",
    userIsNotSelectedProperly: "کاربر مورد نظر به درستی انتخاب نشده است.",
    firstResponsibleIsRequiredForNextUsers: "مسئول اول، پیش نیاز مسئولین بعدی است!",
    secondResponsibleIsRequiredForNextUsers: "مسئول دوم، پیش نیاز مسئولین بعدی است!",
    thirdResponsibleIsRequiredForNextUsers: "مسئول سوم، پیش نیاز مسئولین بعدی است!",
    fourthResponsibleIsRequiredForNextUsers: "مسئول چهارم، پیش نیاز مسئولین بعدی است!",
    setTimesheetResponsibles: "تنظیم مسئولین تایید تایم شیت",
    setVacationResponsibles: "تنظیم مسئولین تایید مرخصی",
    vacationTotalDuration: "مجموع زمان مرخصی",
    waitingForConfirmation: "در انتظار تایید",
    confirmed: "تایید شده",
    rejected: "رد شده",
    allFieldsAreRequired: "وارد کردن تمام موارد ضروری است!",
    addNewVacation: "ثبت مرخصی جدید",
    editVacation: "ویرایش مرخصی",
    vacationAddedSuccessfully: 'مرخصی با موفقیت ایجاد شد',
    vacationEdittedSuccessfully: 'تغییرات با موفقیت ذخیره شد',
    vacationTime: "زمان مرخصی",
    confirmationStatus: "وضعیت تایید",
    confirmationDate: "تاریخ تایید",
    confirmRejectWindow: "پنجره ی تایید یا رد مرخصی",
    vacationWasUpdatedTryAgain: "اطلاعات مرخصی بروزرسانی شد. لطفا مجددا جهت بررسی اقدام فرمایید.",
    totalWaitingForAcceptVacation: "مجموع مرخصی های در انتظار تایید",
    totalAcceptedVacation: "مجموع مرخصی های تایید شده",
    totalRemainedVacation: "مجموع مرخصی های مانده",
    remainedVacationOnVacationCreation: "مانده مرخصی هنگام ثبت مرخصی",
    vacationDetail: "مشخصات مرخصی",
    myVacations: "مرخصی‌های من",
    vacationRejectCause: "علت رد مرخصی",
    otherInfo: "سایر توضیحات",
    detailsOfThisVacation: "جزئیات این مرخصی",
    acceptHistory: "تاریخچه ی تایید",
    vacationsReport: "آمار مرخصی ها",
    vacationType: "نوع مرخصی",
    vacationTotalTime: "مجموع زمان مرخصی",
    vacationCreationTime: "زمان ایجاد مرخصی",
    vacationLastModificationTime: "زمان آخرین تغییر روی مرخصی",
    responsible: "مسئول",
    inPreposition: "در",
    step: "مرحله",
    by: "توسط",
    unableToDeleteCheckedLeave: "امکان حذف مرخصی بررسی شده وجود ندارد.",
    illegal: "غیرمجاز",
    startDate: "تاریخ شروع",
    endDate: "تاریخ پایان",
    startHour: "ساعت شروع",
    endHour: "ساعت پایان",
    type: "نوع",
    confirm: "تایید",
    reject: "رد",
    reportType: "نوع گزارش",
};

ResourceStrs = {
    Name: "نام ",
    ResourceTypeName: "نوع منبع",
    ResourceGroupName: "گروه",
    Description: "شرح نوع منبع",
    StandardRate: "نرخ استاندارد",
    NewResource: "منبع جدید",
    EditResource: "ویرایش منبع",
    FromDate: "تاریخ از",
    ToDate: "تاریخ تا",
    OverTimeRate: "نرخ اضافه کاری",
    CostPerUse: "هزینه بکار گیری",
    NotCompelete: "اطلاعات ناقص تکمیل شده",
    AddResource: "اختصاص منابع",
    value: "مقدار",
    Time: "روز",
    titleresourceassign: "وضعیت تخصیص منبع",
    relatedResourcesCount: "تعداد منابع مرتبط",
    resourceGroupManagement: "مدیریت گروه منبع"
};

cartableStrs = {
    Actionable: "اقدامات",
    CreatedByMe: "ایجاد شده بوسیله من",
    Viewable: "قابل مشاهده	"
};

unitStrs = {
    unitType: "نوع واحد",
    unit: "واحد",
    baseCurrencyAmount: "مقدار ارز پایه",
    name: "نام",
    type: "نوع",
    baseUnitMultiplier: "ضریب نوع مبنا",
    baseUnitOffset: "انحراف از مبنا",
};

generalUnitTypes = [
    { ID: 1, Name: "Length", LocalName: "طول" },
    { ID: 2, Name: "Time", LocalName: "زمان" },
    { ID: 3, Name: "Area", LocalName: "مساحت" },
    { ID: 4, Name: "Volumn", LocalName: "حجم" },
    { ID: 5, Name: "Currency", LocalName: "ارز" },
    { ID: 6, Name: "Mass", LocalName: "جرم" },
    { ID: 7, Name: "Temperature", LocalName: "دما" },
    { ID: 8, Name: "Electric Current", LocalName: "جریان الکتریکی" },
    { ID: 9, Name: "Amount of Substance", LocalName: "مقدار ذره" },
    { ID: 10, Name: "Luminous Intensity", LocalName: "مقدار نور" },
    { ID: 11, Name: "Count", LocalName: "تعداد" },
];

generalUnits = [];

generalUnits[1] =
    [
        { ID: 1, Abbriviation: "M", LocalName: "متر" },
        { ID: 17, Abbriviation: "km", LocalName: "کیلومتر" },
    ];
generalUnits[2] =
    [
        { ID: 2, Abbriviation: "sec", LocalName: "ثانیه" },
        { ID: 14, Abbriviation: "min", LocalName: "دقیقه" },
        { ID: 16, Abbriviation: "hor", LocalName: "ساعت" },
        { ID: 18, Abbriviation: "day", LocalName: "روز" },
        { ID: 19, Abbriviation: "week", LocalName: "هفته" },
        { ID: 20, Abbriviation: "mo", LocalName: "ماه" },
    ];

generalUnits[3] =
    [
        { ID: 4, Abbriviation: "sq m", LocalName: "متر مربع" },

    ];

generalUnits[4] =
    [
        { ID: 5, Abbriviation: "cu m", LocalName: "متر مکعب" },
        { ID: 26, Abbriviation: "ltr", LocalName: "لیتر" },

    ];

generalUnits[5] =
    [
        { ID: 6, Abbriviation: "USD", LocalName: "دلار" },
        { ID: 12, Abbriviation: "IRR", LocalName: "ریال" },
        { ID: 13, Abbriviation: "EUR", LocalName: "یورو" },

    ];

generalUnits[6] =
    [
        { ID: 7, Abbriviation: "kg", LocalName: "کیلوگرم" },
        { ID: 21, Abbriviation: "ton", LocalName: "تن" },
    ];

generalUnits[7] =
    [
        { ID: 8, Abbriviation: "c", LocalName: "سانتیگراد" },
    ];
generalUnits[8] =
    [
        { ID: 9, Abbriviation: "amp", LocalName: "آمپر" },
    ];
generalUnits[9] =
    [
        { ID: 10, Abbriviation: "mole", LocalName: "مول" },
    ];
generalUnits[10] =
    [
        { ID: 11, Abbriviation: "cd", LocalName: "کندل" },
    ];
generalUnits[11] =
    [
        { ID: 22, Abbriviation: "num", LocalName: "عدد" },
        { ID: 23, Abbriviation: "u", LocalName: "واحد" },
        { ID: 24, Abbriviation: "set", LocalName: "دست" },
        { ID: 25, Abbriviation: "pcs", LocalName: "قطعه" },

    ];

CalendarWeekDayNames = ["یکشنبه", "دوشنبه", "سه شنبه", "چهارشنبه", "پنجشنبه", "جمعه", "شنبه"];
CalendarMonthNames = [
    { ID: 1, Abbreviation: "", LocalName: "فروردین" },
    { ID: 2, Abbreviation: "", LocalName: "اردیبهشت" },
    { ID: 3, Abbreviation: "", LocalName: "خرداد" },
    { ID: 4, Abbreviation: "", LocalName: "تیر" },
    { ID: 5, Abbreviation: "", LocalName: "مرداد" },
    { ID: 6, Abbreviation: "", LocalName: "شهریور" },
    { ID: 7, Abbreviation: "", LocalName: "مهر" },
    { ID: 8, Abbreviation: "", LocalName: "آبان" },
    { ID: 9, Abbreviation: "", LocalName: "آذر" },
    { ID: 10, Abbreviation: "", LocalName: "دی" },
    { ID: 11, Abbreviation: "", LocalName: "بهمن" },
    { ID: 12, Abbreviation: "", LocalName: "اسفند" },
];
generalActive = ["غیر فعال", "فعال"];

calendarManagement = {
    grid_Name: "نام",
    grid_Description: "شرح",
    grid_CalenderExceptionsCount: "تعداد روزهای استثنا",
    ewin_title: "ویرایش تقویم پروژه",
    awin_title: "ایجاد تقویم پروژه جدید",
    copywin_title: "ایجاد کپی از تقویم",
    timePeriodConjunction: "تا",

    weekDayGrid_dayOfWeek: "روز هفته",
    weekDayGrid_workingTime: "ساعات کاری",
    weekDayGrid_editWorkingTime: "ویرایش",
    aexceptionwin_title: "افزودن استثناء",
    eexceptionwin_title: "ویرایش استثناء",

    exceptionsGrid_from: "از",
    exceptionsGrid_to: "تا",
    exceptionsGrid_description: "شرح",

    outerTimePeriodConjunction: "و",

    weekDayTimePeriodEditorWindowPrefix: "ویرایش ساعات کاری برای روز ",
    error_exceptions_periodOverlap: "خطا در ویرایش استثناء: بازه های زمانی با یکدیگر همپوشانی دارند ",
    error_workingtime_periodOverlap: "خطا در ویرایش ساعات کاری: بازه های زمانی با یکدیگر همپوشانی دارند",
    error_exceptions_startIsAfterEnd: "تاریخ شروع نباید پس از تاریخ پایان باشد",
    error_fromIsNeeded: "وارد کردن تاریخ شروع الزامیست",
    error_toIsNeeded: "وارد کردن تاریخ پایان الزامیست",
};

tenderManagement =
{
    grid_Title: "عنوان مناقصه",
    grid_Status: "وضعیت چرخه تایید ",
    grid_ProjectName: "پروژه",
    grid_Permissions: "دسترسی ها",

    grid_DurationDays: "طول مدت پروژه",
    grid_WaterAndPowerByTaskMaster: "پرداخت هزینه آب و برق بر عهده کارفرما",
    grid_UseTaskMasterEquipment: "امکان استفاده از ماشین آلات کارفرما توسط پیمانکار",
    grid_MaterialByTaskMaster: "تهیه مصالح عمده مصرفی",
    grid_MajorMaterialByTaskMaster: "تهیه مصالح عمده غیر مصرفی",
    grid_ProjectPlace: "محل پروژه",
    grid_AccommodationByTaskMaster: "امکان اسکان پرسنل پیمانکار در پروژه",
    grid_EstimatedAmount: "مبلغ برآورد اولیه",
    grid_DateAccepted: "تاریخ تایید",
    grid_PotensialVendorsCount: "تعداد افراد و شرکتهای واجد شرایط",
    grid_Duration: "مدت انجام کار",
    grid_DailyDelayPenalty: "جریمه روزانه تاخیرات غیر مجاز",

    awin_title: "ثبت پیشنویس مناقصه",
    ewin_title: "ویرایش پیشنویس مناقصه",


    tenderOfferNo: "شماره",
    tenderHoldingDate: "تاریخ برگزاری",
    tenderExclusiveTerms: "شرایط اختصاصی",
    tenderRunType: "نحوه اجرا",
    tenderScope: "محدوده اجرا",
    tenderType: "نوع مناقصه",
    envelopeDeliveryDate: "تاریخ تحویل پاکت",
};

priceInqueryRequestManagementStrs =
{
    grid_Title: "عنوان درخواست استعلام قیمت",
    grid_Status: "وضعیت چرخه تایید ",
    grid_ProjectName: "پروژه",
    grid_Permissions: "دسترسی ها",

    grid_Description: "شرح",
    grid_DateAccepted: "تاریخ تایید",

    awin_title: "ثبت درخواست استعلام قیمت",
    ewin_title: "ویرایش درخواست استعلام قیمت",
};

contractDraftManagementStrs = {

    grid_Title: "عنوان پیشنویس قرارداد",
    grid_Status: "وضعیت چرخه تایید ",
    grid_ProjectName: "پروژه",
    grid_Permissions: "دسترسی ها",

    grid_EstimatedAmount: "مبلغ برآورد اولیه",
    grid_PerformanceGuarantee: "رقم تضمین تعهدات",
    grid_GuaranteeDuration: "دوره تضمین",
    grid_PrePayment: "پیش پرداخت",
    grid_SubContractorName: "پیمانکار",
    grid_ContractDuration: "مدت قرارداد",
    grid_DateAccepted: "تاریخ تایید",
    grid_DailyDelayPenalty: "جریمه روزانه تاخیر",

    awin_title: "ثبت پیشنویس قرارداد",
    ewin_title: "ویرایش پیشنویس قرارداد",
};

w_dfiDelayReasonPieChartStrs = {
    notSet: "[بدون تاخیر غیر مجاز]",
    settings_displayBy: "نمایش بر اساس",
    settings_displayByCount: "تعداد",
    settings_displayByPercent: "درصد"
};

workflowStatusStrs = {
    notStarted: "[شروع نشده]"
};

w_invoicesStatusStrs = {
    settings_displayBy: "نمایش بر اساس",
    settings_displayByCount: "تعداد",
    settings_displayByAmount: "مبلغ",
    title_count: "آمار تعداد صورت وضعیتها بر اساس مرحله کنونی",
    title_amount: "آمار جمع مبالغ صورت وضعیتها بر اساس مرحله کنونی",
};

w_projectStatisticsStrs = {
    remainingDays: "روزهای باقی مانده",
    allDataFormItemsCount: " کل فعالیتهای پروژه",
    delayedNotStartedDataFormItemsCount: " فعالیتهای تاخیر یافته",
    criticalDataFormItemsCount: " فعالیتهای با وضعیت حاد",
    nextWeekDataFormItemsCount: "فعالیتهای هفته آتی",
    day: "روز",
    activity: "فعالیت",
};

w_scurveStrs = {
    plan: "برنامه ای",
    actual: "واقعی",
    planCommulative: "برنامه ای تجمعی",
    actualCommulative: "واقعی تجمعی",
    commulative: "تجمعی",
    totalWeightedAverage: "میانگین وزنی",
    sCurveOnWBS: "نمودار S روی WBS",
    sCurve: "نمودار S",
    comparePlansScurve: "مقایسه برنامه های پروژه",
};

w_subWbsStatusStrs = {
    plan: "برنامه",
    actual: "واقعی",
    planEnd: "پایان برنامه ای",
    planStart: "شروع برنامه ای",
    actualStart: "شروع واقعی",
    actualEnd: "پایان واقعی",
    grid_phaseName: "عنوان فاز",
    gird_progress: "پیشرفت",
    grid_planActual: "برنامه/واقعی",
    grid_manHour: "نفرساعت",
    sumOfPlanHour: "مجموع برنامه ریزی شده",
    showManHour: "نمایش اطلاعات نفر ساعت",
};

w_weeklyPlanStrs = {
    thisWeekActivities: "فعالیتهای هفته جاری",
    nextWeekActivities: "فعالیتهای هفته آتی",
    thisWeekTitle: "فعالیتهایی که بر اساس برنامه در این هفته شروع میشوند یا پایان میپذیرند.",
    nextWeekTitle: "فعالیتهایی که بر اساس برنامه در هفته آتی شروع میشوند یا پایان میپذیرند."
};

w_projectListStrs = {
    plan: "برنامه",
    actual: "واقعی",
    planEnd: "پایان برنامه",
    planStart: "شروع برنامه",
    actualStart: "شروع واقعی",
    actualEnd: "پایان واقعی",
    grid_title: " عنوان پروژه",
    gird_progress: "پیشرفت",
    grid_planActual: "برنامه/واقعی",
    grid_cost: "هزینه",
    grid_earnedValue: "ارزش کسب شده",

    setting_totalBudgetCalculationType: "روش محاسبه ارزش برنامه ای",
    setting_totalBudgetCalculationType_projectTotalPlannedCashOut: "بر اساس مجموع برنامه جریان نقدی خروجی  پروژه",
    setting_totalBudgetCalculationType_projectCBSRootBudjet: "بر اساس مجموع بودجه سطح صفر CBS",

    setting_currentPlanValueCalculationType: "روش محاسبه ارزش برنامه ای کنونی",
    setting_currentPlanValueCalculationType_plannedCashOut: "بر اساس مجموع برنامه جریان نقدی خروجی تا کنون",
    setting_currentPlanValueCalculationType_totalBudgetXActualProgress: "بودجه کل ضرب در میزان پیشرفت",

    setting_currentActualCostCalculationType: "روش محاسبه هزینه واقعی کنونی",
    setting_currentActualCostCalculationType_totalPayements: "بر اساس مجموع پرداختها تا کنون",
    setting_currentActualCostCalculationType_totalInvoiceFinalAcceptedAmounts: "بر اساس مجموع مبالغ تایید شده صورت وضعیت",
    setting_currentActualCostCalculationType_totalActualResourceCosts: "بر اساس هزینه واقعی منابع"
};

w_projectLocationOnGoogleMapStrs = {
    alert_noLocation: "پروژه دارای طول و عرض جغرافیایی معتبر نیست",
    projectMainLocation: "محل اصلی استقرار پروژه"
};

w_myProjectsStatusStrs = {
    statusName: "عنوان وضعیت",
    count: "تعداد",
    percent: "درصد"
};


w_dfiDelayReasonPieChartStrs = {
    count: "تعداد",
    percent: "درصد"
};

projectResource = {
    contractNO: "شماره قرارداد",
    taskMaster: "کارفرما",
    contractor: "پیمانکار",
    projectNo: "کد پروژه",
    projectName: "عنوان پروژه",
    supervisor: "ناظر",
    projectType: "نوع پروژه",
    contractSubject: "موضوع قرارداد",
    projectExecutionPlace: "محل اجرای پروژه",
    servicesDescription: "شرح خدمات",
    latitude: "عرض",
    longitude: "طول",
    projectStatus: "وضعیت پروژه",
    projectCity: "شهر اجرای پروژه",
    projectLocation: "محل اجرای پروژه",
    timeAndWbsInfo: "اطلاعات زمانی و شکست کار پروژه",
    timeInfo: "اطلاعات زمانی پروژه",
    newProjectPlan: "برنامه زمانبندی جدید",
    editProjectPlan: "ویرایش برنامه زمانبندی",
    delProjectPlan: "حذف برنامه زمانبندی",
    delProjectPlanConfirm: "از حذف برنامه زمامبندی پروژه اطمینان دارید؟",
    newProjectWBS: "شکست کار جدید",
    editProjectWBS: "ویرایش شکست کار",
    delProjectWBS: "حذف شکست کار",
    delProjectWBSConfirm: "از حذف شکست کار پروژه اطمینان دارید؟",
    cannotDeleteProjectWBS: "به علت استفاده به عنوان شکست کار پیش فرض، قادر به حذف نیستید",
    cannotDeleteProjectPlan: "به علت استفاده به عنوان زمانبندی پیش فرض، قادر به حذف نیستید",
    manageProjectPlans: "مدیریت برنامه ها",
    manageProjectWBSs: "مدیریت شکست کارها",
    projectDateType: "نوع تاریخ پروژه",
    projectTimeUnit: "نوع نمایش مدت زمان فعالیتها",
    hoursPerDay: "ساعت کاری در روز",
    daysPerWeek: "روز کاری در هفته",
    daysPerMonth: "روز کاری در ماه",
    contractDate: "تاریخ قرارداد",
    contractEffectDate: "تاریخ ابلاغ قرارداد",
    planStartDate: "تاریخ شروع برنامه ریزی شده",
    planEndDate: "تاریخ پایان برنامه ریزی شده",
    actualStartDate: "تاریخ واقعی شروع",
    actualEndDate: "تاریخ واقعی پایان",
    forecastEndDate: "تاریخ پیش بینی پایان",
    defaultProjectWBS: "شکست کار پیشفرض پروژه",
    defaultProjectPlan: "برنامه زمانبندی پیشفرض",
    allowSelectDateForActualProgress: "امکان ثبت تاریخ در پیشرفت واقعی",
    projectManager: "راهبر پروژه",
    responsibilities: "مسئولیت ها",
    projectDefaultCurrency: "ارز پیش فرض پروژه",
    projectPublicSettings: "تنظیمات شناسنامه ای پروژه",
    geographicalLocation: "موقعیت جغرافیایی",
    projectContract: "قرارداد پروژه",
    anotherSettings: "تنظیمات دیگر",
    customFields: "فیلدهای سفارشی",
    itemlink: "تعیین روش های محاسبه پیشرفت برنامه آیتم های پروژه",
    Consultant: "مشاور",
    DefaultProjectCalendar: "تقویم پیش فرض پروژه",
};

workflowDesignerStrs = {
    Title: "نام",
    Description: "شرح",
    NoStartState: "چرخه میبایست دارای یک حالت شروع باشد.",

    nextStep: "مرحله بعد",
    previousStep: "مرحله قبل",
    commentStep: "کامنت",
    staticCommulativeProgressPercent: "مقدار ثابت درصد تجمعی",
    userCanEnterCommulativeProgressPercent: "امکان تعیین درصد تجمعی بوسیله کاربر",
    isInvoiceFinalAcceptanceState: "تایید صورت وضعیت در این مرحله به پایان میرسد",
    isFinalAcceptanceState: "تایید رسمی در این مرحله به پایان میرسد",
    isFormalAcceptanceState: "تایید رسمی",
    invoiceStep_acceptText: "عنوان تایید",
    invoiceStep_stepText: "متن مرحله",
    stepText: "متن مرحله",
    from: "از",
    to: "تا",
    commulativePhysicalProgressPercent: "میزان پیشرفت فیزیکی",
    commulativeFinancialProgressPercent: "میزان پیشرفت مالی",
    commulativeInvoiceProgressPercent: "میزان پیشرفت صورت وضعیت",
    acceptProgressAutomaticallyInThisStep: "تایید پیشرفت در این مرحله به صورت خودکار",
    stepNameInProgress: "نام مرحله در پیشرفت برنامه ای",
    allowTransmital: "امکان الصاق به مرسوله در این مرحله",
    allowIDC: "امکان چرخش داخلی در این مرحله",
    autoReferByRefrenceMatrix: "ارجاع خودکار بر اساس ماتریس ارجاع",
    physicalProgress: "پیشرفت فیزیکی",
    invoiceProgress: "پیشرفت صورت وضعیت",
    financialProgress: "پیشرفت مالی",
    physicalProgressPeriodIsNotValid: "بازه پیشرفت فیزیکی به درستی مقدار داده نشده است",
    financialProgressPeriodIsNotValid: "بازه پیشرفت صورت وضعیت به درستی مقدار داده نشده است",
    invoiceProgressPeriodIsNotValid: "بازه پیشرفت صورت وضعیت به درستی مقدار داده نشده است",
    fieldValue: "مقدار فیلد",
    fieldValueShouldBeInRange: "میبایست بین 0 تا 100 باشد",
    fieldValueIsNotValid: "معتبر نیست",
    acceptancePathTitle: "عنوان مسیر تایید",

    stateSettings: "تنظیمات مرحله",
    statePermissions: "دسترسیها",
    stateEventSettings: "تنظیمات رخدادها",
    stateAcceptancePaths: "مسیرهای تایید",

    parameterDataType_String: "رشته",
    parameterDataType_Int: "عدد صحیح",
    parameterDataType_Int64: "عدد صحیح بزرگ",
    parameterDataType_Double: "عدد اعشاری",
    parameterDataType_Boolean: "صحیح/غلط",

    parameterManager_winTitle: "پارامترهای تصمیم گیری",
    parameterDataType_Type: "نوع پارامتر",
    parameterManager_parameterName: "نام پارامتر",
    parameterManager_parameterTitle: "عنوان پارامتر",
    parameterManager_add: "ثبت پارامتر جدید",
    parameterManager_edit: "ویرایش پارامتر",

    parameterAlreadyAdded: "این پارامتر پیش از این اضافه شده است",

    permissionManager_roles: " نقشهای پروژه موجود",
    permissionManager_assignedAccesses: "دسترسیهای تخصیص یافته",
    permissionManager_notAssignedAccesses: "دسترسیهای تخصیص نیافته",

    stateEventSetting_title: "عنوان",
    stateEventSetting_description: "شرح",
    stateEventSetting_arguments: "آرگومان",
    stateEventSetting_groupType: "نوع گروهبندی",
    stateEventSetting_commentIsRequired: "اجباری بودن توضیحات",
    stateEventSetting_acceptProgress: "تایید پیشرفت",

    stateTypes_start: "شروع",
    stateTypes_branchState: "تصمیم گیری خودکار",
    stateTypes_multiUserChoiseState: "تصمیم گیری کاربر",
    stateTypes_parallelAcceptanceState: "تایید موازی",
    stateTypes_dynamicParallelAcceptanceState: "تایید موازی پویا",
    stateTypes_uTurnState: "دور برگردان",
    stateTypes_finish: "پایان",
    stateTypes_comment: "توضیحات",
    stateTypes_generalDataFormItemState: "مرحله تاییدیه قلم کاری",
    stateTypes_invoiceApprovalState: "مرحله تاییدیه صورت وضعیت",
    stateTypes_simpleAcceptRejectState: "مرحله تاییدیه ساده",
    stateTypes_documentApprovalState: "مرحله تاییدیه مدرک مهندسی",
    stateTypes_procurementItemState: "مرحله تاییدیه آیتم خرید",

    parallel_minAccept: "حداقل تایید",
    parallel_minReject: "حداقل رد",

    branch_value1NotValid: "با توجه به نوع اپراتور انتخاب شده، مقدار وارد شده برای 'مقدار1' معتبر نمیباشد",
    branch_value2NotValid: "با توجه به نوع اپراتور انتخاب شده، مقدار وارد شده برای 'مقدار2' معتبر نمیباشد",
    branch_parameter: "پارامتر",
    branch_operator: "اپراتور",
    branch_value1: "مقدار 1",
    branch_value2: "مقدار 2",

    permissionsAreDisciplineWise: "دسترسی ها بر اساس دیسیپلین هستند",
    acceptancePathDesign_editAcceptancePath: "ویرایش مسیرهای تایید",
    acceptancePathDesign_roles: "نقشها",
    rolesSelector_notAssignedRoles: "نقشهای تخصیص نیافته",
    rolesSelector_assignedRoles: "نقشهای تخصیص یافته",
    branchStateSettings_condistionState: 'مرحله بر اساس شرط',
    branchStateSettings_exit: 'خروج',
    dynamicParallelAcceptanceStateSettings_accept: 'تایید/مرحله بعد',
    dynamicParallelAcceptanceStateSettings_reject: 'رد/ مرحله قبل',
    dynamicParallelAcceptanceStateSettings_comment: 'کامنت/ تغییرات',

    multiUserChoiseStateSetting_decisionTitle: "عنوان تصمیم",
    multiUserChoiseStateSetting_decisionDescription: "شرح تصمیم",
    multiUserChoiseStateSetting_conditionState: "مرحله بر اساس شرط",

    relatedDataModels: 'مدلهای داده ای مرتبط',
    assignDataModel: 'تخصیص',
    rowOrder: 'ردیف',
    isPredefined: "پیش فرض سیستم",
    modelName: 'نام مدل داده',

    noParam: 'پارامتری جهت تصمیم گیری اضافه نشده است',
    dataModelManagement: 'مدیریت مدلهای داده ای',
};

securityStrs = {
    permissionName: "نام دسترسی",
    roleNames: "نام نقشها",
    projectRoleDeleteConfirm: "لطفا از بکار گرفته نشدن این نقش در چرخه ها اطمینان حاصل کنید. آیا از حذف این نقش در پروژه مطمئن هستید؟",
    projectRoleNameDuplicated: "نام وارد شده تکراری است",
    noAccessToThisRab: "شما به این صفحه دسترسی ندارید",
    log_actionName: 'نوع رخداد',
    log_time: 'زمان رخداد',
    log_browser: 'مرورگر',
    log_user: 'کاربر',
    log_os: 'سیستم عامل',
    log_ip: 'IP',
    log_client: 'سیستم کاربر',
    log_userRole: 'نقش کاربر',
    log_details: 'جزئیات',
    log_requestAddress: 'آدرس درخواست',
    log_responseStatusCode: 'کد وضعیت پاسخ',
    mngUser_username: 'نام کاربری',
    mngUser_firstName: 'نام',
    mngUser_lastName: 'نام خانوادگی',
    mngUser_systemRole: 'نقش سیستمی',
    mngUser_isActive: 'فعال',
    mngUser_stakeholder: 'ذینفع',
    mngUser_post: 'سمت',
    mngUser_email: 'ایمیل',
    mngUser_emailConfirmed: 'ایمیل تایید شده است',
    mngUser_mobileNumber: 'موبایل',
    mngUser_mobileNumberConfirmed: 'موبایل تایید شده است',
    mngUser_dateTypeName: 'نوع تاریخ',
    mngUser_accessFailedCount: 'تلاش ناموفق قبل از قفل',
    mngUser_previousAccessFailedCount: 'تلاش ناموفق قبل از آخرین تلاش موفق',
    mngUser_twoFactorEnabled: 'نیاز به تایید دو مرحله ای',
    mngUser_lockoutEndDate: 'زمان انتهای قفل',
    mngUser_lastLoginTime: 'زمان آخرین ورود موفق',
    mngUser_lastFailedLoginTime: 'زمان آخرین ورود ناموفق',
    mngUser_lastLoginIP: 'آدرس آخرین ورود موفق',
    mngUser_sessionsCount: 'تعداد نشست فعال',
    mngUser_latestLoginFailed: 'آخرین تلاش ناموفق',
    mngUser_hasClientCertificate: 'امضای دیجیتال دارد',
    mngUser_lockoutEnabled: 'فعال بودن قفل شدن',
    mngUser_signature: 'امضا',
    mngUser_newKeyPassCreated: 'عملیات با موفقیت انجام شد. گذرواژه کلید جدید ایجاد شده',
    mngUser_newUser: 'کاربر جدید',
    mngUser_editUser: 'کاربر جدید',
    mngUser_passwordsIsNotSame: 'گذرواژه های انتخاب شده یکسان نیستند',
    mngUser_dateTime: 'زمان',
    mngUser_browser: 'مرورگر',
    mngUser_os: 'سیستم عامل',
    mngUser_IP: 'IP',
    mngUser_clientName: 'سیستم کاربر',
    mngUser_userRole: 'نقش کاربر',
    mngUser_details: 'جزئیات',
    mngUser_requestContentType: 'نوع محتوای درخواست',
    mngUser_requestUri: 'آدرس درخواست',
    mngUser_requestMethod: 'متد درخواست',
    mngUser_requestRouteTemplate: 'ساختار مسیر درخواست',
    mngUser_responseContentType: 'نوع محتوای پاسخ',
    mngUser_responseStatusCode: 'کد وضعیت پاسخ',
    mngUser_responseTimestamp: 'زمان پاسخ',
    mngUser_UserHasDigitalSignatureMessage: 'کاربر انتخاب شده در حال حاضر دارای امضای دیجیتال است. در صورت ایجاد مجدد، امضای قبلی نا معتبر خواهد شد',
    mngUser_usernameDuplicate: 'نام کاربری انتخاب شده تکراریست',
    mngUser_usernameDuplicateAndDeleted: 'نام کاربری انتخاب شده قبلا انتخاب شده و حذف شده است. برای حفظ یکپارچگی اطلاعات نمیتوانید مجددا از این نام کاربری استفاده کنید',
    mngUser_usernameCharactedShouldNumAndEnChar: 'نام کاربری تنهای میتواند از کارکترهای لایتین و اعداد تشکیل شده باشد',
    mngUser_passwordMustHasNum: 'گذرواژه میبایست حتما دارای یک کارکتر عددی باشد',
    mngUser_passwordMustHasNonNumChar: 'گذرواژه میبایست حتما دارای یک کارکتر غیر عددی و حرفی باشد',
    mngUser_passwordMustHasLowerCaseChar: 'گذرواژه میبایست حتما دارای یک کارکتر حرف کوچک لاتین باشد',
    mngUser_passwordMustHasUpperCaseChar: 'گذرواژه میبایست حتما دارای یک کارکتر حرف بزرگ لاتین باشد',
    mngUser_passwordAndUsernameShouldNotSame: 'نام کاربری و گذرواژه نمیتوانند یکسان باشند',
    mngUser_passwordIsSameWithBefore: 'گذرواژه وارد شده با قبل یکسان است',
    mngUser_emailIsEmptyOrDuplicate: 'ایمیل وارد نشده و یا تکراریست',
    mngUser_mobileIsEmptyOrDuplicate: 'تلفن موبایل وارد نشده و یا تکراریست',
    mngUser_passwordCharNumber: 'گذرواژه میبایست حداقل {0} کاراکتر باشد ',
    mngUser_passwordIncorrect: 'گذرواژه نامعتبر',


};


authenticationSettingsStrs = {
    cantSetUniqueEmail: "امکان ست کردن مقدار 'ایمیل میبایست غیر تکراری باشد' وجود ندارد زیرا هم اکنون کاربرانی بدون ایمیل و یا با ایمیل تکراری در سیستم وجود دارند",
    loginDataValidTimeNotValid: "مقدار 'زمان معتبر بودن اطلاعات ورود' نامعتبر است. میبایست از 0 بزرگتر باشد",
    minUserLockedMinutesNotValid: "مقدار 'زمان ماندن کاربر در حالت قفل' نامعتبر است. حداقل 1 دقیقه",
    minFailedLogginBeforLogNotValid: "مقدار 'تعداد تلاش ناموفق برای قفل شدن' نامعتبر است. حداقل: 2",
    minLengthNotValid: "مقدار 'حداقل طول' نامعتبر است. حداقل :3",
    passwordValidTimeNotValid: "مقدار 'زمان معتبر بودن گذرواژه' نامعتبر است. حداقل: 0",
    minFailedLoginBeforeEntringTwoFactorNotValid: "مقدار 'تعداد تلاش ناموفق برای ورود به شناسایی دو مرحله ای' نامعتبر است. حداقل : 2",
    lockoutAndTwoFactorDoesNotMeetWithEachOther: "در صورت فعال بودن همزمان قفل شدن کاربر و شناسایی دو مرحله ای، تعداد دفعات شناسایی دو مرحله ای میبایست کمتر باشد",
    uniqueMobileNumberCanNotBeSet: "امکان ست کردن مقدار 'تلفن موبایل میبایست غیر تکراری باشد' وجود ندارد زیرا هم اکنون کاربرانی بدون تلفن موبایل و یا با تلفن موبایل تکراری در سیستم وجود دارند",
    loggedInUsersDataCheckTimeFrame: "مقدار 'بازه زمانی بررسی اعتبار کاربران لوگین کرده' نا معتبر است. حداقل: 0",
};

ProjectPlanning = {
    WBS: "شکست کار",
    defaultWBS: "شکست کار پیش فرض",
    defaultProjectPlan: "برنامه زمانبندی پیش فرض",

    planprogresscalculationheadtab: "محاسبه پیشرفت برنامه ای",
    weightingheadtab: "وزندهی",
    schedulingheadtab: "زمانبندی",

    planprogressrecalculationMessage: "جهت به روز رسانی پیشرفت برنامه ای پروژه بر اساس برنامه زمانبندی انتخاب شده کلیک نمایید",
    PlanProgressCalculationButtonCaption: "محاسبه مجدد",

    Timeconsumingprocess: "توجه داشته باشید که این فرایند میتواند زمان زیادی را از سرور بگیرد",

    weightingBottomUp: "وزندهی از پایین به بالا",
    weightingBottomUpMessage: "با استفاده از این گزینه وزن تمامی سطوح WBS برابر با جمع اوزان اقلام کاری زیر مجموعه آنها قرار خواهد گرفت ",
    weightingBottomUpButtonCaption: "انجام وزندهی",
    weightingTopBottomButtonCaption: "انجام وزندهی",

    weightingTopDown: "وزندهی از بالا به پایین",
    weightingTopDownMessage: "با استفاده از این گزینه وزن تمامی اقلام کاری برابر با وزن فعالیت تقسیم بر تعداد اقلام کاری زیر مجموعه آن فعالیت قرار خواهد گرفت",

    schedulingTopDown: "زمانبندی از بالا به پایین",
    schedulingTopDownMessage: "این فعالیت زمانبندی شروع و پایان اقلام کاری را بر اساس شروع و پایان فعالیت بالاسری تنظیم می کند.",
    schedulingTopDownButtonCaption: "انجام زمانبندی",

    actionResultpreMessage: "محاسبه برنامه زمانی برای تعداد {0} آیتم با موفقیت انجام شد",
    ProcessDuration: "زمان فرآیند",
    midTotalWeightMessage: "",
    TotalWeightMessage: "جمع اوزان اقلام کاری پروژه {0} در زمانبندی انتخاب شده برابر با {1} است.",
    TotalWeightAlertMessage: "در صورت عدم اصلاح وزن اقلام کاری گزارشات پیشرفت غیر صحیح خواهند بود."
};

ProjectContractCashflow = {
    ProjectTitle: "عنوان پروژه",
    Actual: "واقعی",
    Program: "برنامه",
    Total: "جمع",
    ContractType: "انواع قرارداد",
    ContractBudgetChart: "نمودار بودجه قرارداد",
    FillCacheContractCashflowCaption: "بازسازی اطلاعات",
    ShowResultCaption: "نمایش نتیجه گزارش",
    Year: "سال"
};
ContractCashflow = {
    ContractTitle: "عنوان قرارداد",
    Actual: "واقعی",
    Program: "برنامه",
    Total: "جمع",
    ContractType: "انواع قرارداد",
    ContractBudgetChart: "نمودار بودجه قرارداد",
    ShowResultCaption: "نمایش نتیجه گزارش",
    Year: "سال"
};

galleryStrs = {
    manageAlbums: "مدیریت آلبومها",

    aewin_title: "عنوان",
    aewin_album: "آلبوم",
    aewin_description: "توضیحات",
    aewin_showInDashboard: "نمایش در داشبورد",
    aewin_photo: "عکس",
    aewin_photoFile: "فایل عکس",

    albumManagerWinTitle: "مدیریت آلبوم ها",
    addNewPhotoWindowTitle: "افزودن عکس جدید",
    editPhotoWindowTitle: "ویرایش عکس",
    pleaseUploadAPhotoFile: "لطفا یک عکس آپلود نمایید.",
    pictureSelect: "انتخاب تصویر",
    grid_originalFileName: "نام اصلی فایل",
    grid_imageContentType: "نوع عکس",
    preview: "پیش نمایش تصویر",

    albume_title: "عنوان",
    album_description: "توضیحات",
    album_addWinTitle: "افزودن آلبوم جدید",
    album_editWinTitle: "ویرایش آلبوم",
};
userSettingStrs = {
    pleaseSelectOneFile: "لطفا حداکثر یک فایل را انتخاب نمائید",
    editWinTitle: 'ویرایش اطلاعات پروفایل'
};
////////////////////////////////////////////////////// Periodic Report Starts ///////////////////////////////////////////////////////
periodicReportSettings = {
    SelfDeclareReasonMandatoryMarginPercent: "بازه انحراف اجباری بودن ورود علت تاخیر",
    MaximumAttachedFilesCount: "حداکثر تعداد فایل پیوست",
    MaxFileSize: "حداکثر سایز فایل قابل قبول",
    kilobyte: "کیلو بایت",
    AllowedFileExtensions: "فرمتهای فایل قابل قبول (با کاما جدا کنید، * خالی به معنای قبول همه فرمتهاست)",
    ContractPeriodicReportSettingEnable: "نمایش تنظیمات بررسی قرارداد",
    DocumentAttachmentIsMandatoryForSelfDeclare: "اجباری بودن الصاق مستندات حین ثبت خود اظهاری",
    CanSelfDeclareMoreThanPlan: "امکان ثبت پیشرفت واقعی بیش از برنامه",
    AssessVarianceDropDownVisible: "نمایش منوی علل انحراف انطباق",
    AssessVarianceCommentBoxVisible: "نمایش باکس ورود شرح علل انحراف انطباق",
    CanFinalizeSelfDeclareBeforeSelfDeclarePeriodFinished: "امکان نهایی کردن خود اظهاری قبل از اتمام دوره خوداظهاری"
};

acceptReportStrs = {
    contractProgressSaveIDIsRequired: "پارامتر contractProgressSaveID الزامیست",
    notDoneYet: 'هنوز صورت نگرفته است',
    pleaseEnterAssessedProgress: 'لطفا پیشرفت ارزیابی را وارد کنید',
    canNotAssessMoreThanSelfDeclare: 'امکان ثبت ارزیابی بیش از خوداظهاری وجود ندارد',
    canNotSelfDeclareLessThanBefore: 'برای این قلم کاری در در مراحل قبلی ارزیابی پیشرفت {0} ثبت شده است. امکان ثبت پیشرفت بیشتر از این مقدار وجود ندارد ',
    shouldAddReason: 'ورود علت انحراف ارزیابی برای ارزیابی با میزان انحراف بیش از {0} درصد نسبت به خود اظهاری الزامی است',
    shouldReadAllDocumentsBeforeSaving: 'قبل از ثبت ارزیابی مشاهده کلیه مستندات ارسال شده الزامی است',
    errorInSavingProgress: 'مشکلی در ثبت اطلاعات پیشرفت ',
    progressSuccessfullySaved: "پیشرفت با موفقیت ثبت شد",
    toBeInformedAboutNotCheckedItems: "تعداد {0} قلم کاری را هنوز بررسی نکرده اید. در صورت تایید نهایی مقدار پیشرفت این گونه ها برابر با میزان ثبت شده در مرحله {1} در نظر گرفته میشوند  ",
    canNotAcceptAllBeforeAcceptingAll: " تعداد {0} قلم کاری را هنوز بررسی نکرده اید. امکان تایید در این مرحله تنها پس از بررسی کامل وجود خواهد داشت  ",
    acceptReportWPTabs_notAssessedSelfDeclares: "اقلام کاری خود اظهاری شده ارزیابی نشده",
    acceptReportWPTabs_allSelfDeclaredItems: " تمامی اقلام کاری خود اظهاری شده",
    acceptReportWPTabs_activitiesTree: "شکست کار فعالیتها",
    acceptReportWPTabs_notAssessedSelfDeclaresDesc: "لیست تمامی اقلام کاری ثبت خود اظهاری شده که هنوز بوسیله شما ارزیابی نشده اند",
    acceptReportWPTabs_allSelfDeclaredItemsDesc: "لیست تمامی اقلام کاری ثبت خود اظهاری شده",
    acceptReportWPTabs_activitiesTreeDesc: "شکست کار کلیه اقلام کاری خود اظهاری شده",
    nextStepDefaultTitle: 'تایید نهایی و ثبت پیشرفت واقعی',
    nextStepChangedTitle: 'تایید و ارسال برای {0} ',
    level0Title: "خوداظهاری",
};

contractPeriodicReportAcceptanceCartableStrs = {
    grid_ProjectName: "پروژه",
    grid_ContractNumber: "شماره قرارداد",
    grid_Title: "عنوان قرارداد",
    grid_ContractTypeName: "نوع قرارداد",
    grid_ProgressReportsPeriodsTypeTitle: "دوره ورود اطلاعات",
    grid_LatestProgressPeriodCheckDate: "دوره جاری (منتهی به)",
    grid_LatestSavedProgressPeriodCheckDate: "آخرین دوره ذخیره شده (منتهی به)",
    grid_WaitingForAcceptance: "تعداد گزارشات منتظر تایید",
    progressPeriodCheckDateTitle: "دوره (منتهی به)",
    acceptorUser: "کاربر تایید کننده",
    acceptTime: "تاریخ تایید",
};

contractPeriodicReportDFIListStrs = {
    pleaseEnterProgress: 'لطفا پیشرفت را وارد کنید',
    confirmSelfDeclareMoreThanPlan: 'پیشرفت ثبت شده از میزان برنامه بالاتر است. آیا مطمئن هستید که میخواهید این پیشرفت ثبت شود؟',
    canNotSelfDeclareMoreThanPlan: 'امکان ثبت پیشرفتی بیش از درصد پیشرفت برنامه وجود ندارد',
    canNotSelfDeclareMoreThan100: 'امکان ثبت پیشرفتی بیش از صد وجود ندارد',
    canNotSelfDeclareLessThanLatestActualAcceptedProgress: 'امکان ثبت پیشرفتی کمتر از میزان پیشرفت واقعی تایید شده وجود ندارد',
    canNotSelfDeclareLessThanLatestDeclaredProgress: 'امکان ثبت پیشرفتی کمتر از آخرین میزان پیشرفت وارد شده وجود ندارد',
    shouldEnterDelayReason: 'ورود علت تاخیر برای فعالیتهایی با میزان تاخیر بیش از {0} درصد نسبت به پیش بینی الزامی است',
    canNotSelfDeclareWithNoAttachedDocuments: 'هیچ مستندی الصاق نشده است. بدون الصاق مستندات امکان ثبت پیشرفت وجود ندارد.',
    progressSuccessfullySaved: "پیشرفت با موفقیت ثبت شد",
    errorInSavingProgress: 'مشکلی در ثبت اطلاعات پیشرفت ',
    grid_LatestSelfDeclaredProgressPeriodCheckDate: "آخرین دوره اظهار شده",
    grid_LatestDeclaredProgress: "آخرین پیشرفت اظهار شده",
    grid_SelfDeclare: 'ثبت اطلاعات پیشرفت',
    editProgress: "ویرایش پیشرفت",
    saveProgress: "ثبت پیشرفت",
    gettingRootPath: "دریافت مسیر ریشه",
    progressWinTitle: "ثبت پیشرفت",
    progressHistory: "تاریخچه پیشرفت",
    dfiProgressIsAlready100: 'پیشرفت این قلم کاری پیش از این کامل شده است',
    noActiveProgressPeriod: "دوره پایش فعالی وجود ندارد"
};

contractPeriodicReportDFIListForAcceptanceStrs = {
    pleaseEnterProgress: 'لطفا پیشرفت را وارد کنید',
    canNotSelfDeclareMoreThanPlan: 'امکان ثبت پیشرفتی بیش از درصد پیشرفت برنامه وجود ندارد',
    canNotSelfDeclareMoreThan100: 'امکان ثبت پیشرفتی بیش از صد وجود ندارد',
    canNotSelfDeclareLessThanLatestActualAcceptedProgress: 'امکان ثبت پیشرفتی کمتر از میزان پیشرفت واقعی تایید شده وجود ندارد',
    shouldEnterDelayReason: 'ورود علت تاخیر برای فعالیتهایی با میزان تاخیر بیش از {0} درصد نسبت به پیش بینی الزامی است',
    canNotSelfDeclareWithNoAttachedDocuments: 'هیچ مستندی الصاق نشده است. بدون الصاق مستندات امکان ثبت پیشرفت وجود ندارد.',
    progressSuccessfullySaved: "پیشرفت با موفقیت ثبت شد",
    errorInSavingProgress: 'مشکلی در ثبت اطلاعات پیشرفت ',
    grid_SelfDeclare: 'خود اظهاری',
    progressWinTitle: "ثبت پیشرفت",
    notAssessed: 'بررسی نشده',
    assess: "بررسی",
};

dataFormItemHistoryStrs = {
    gettingContractPeriodicReportSettingError: "اشکال در دریافت تنظیمات ",
    grid_ProgressPeriodCheckDate: 'برای دوره منتهی به',
};

progressReportsPeriodsManagementStrs = {
    grid_Title: 'عنوان',
    grid_ProgressPeriodCheckDate: 'برای دوره منتهی به',
    grid_DataGatheringStartDate: "تاریخ شروع خوداظهاری",
    grid_DataGatheringEndDate: "تاریخ پایان خوداظهاری",
    noRows: "هیچ رکوردی یافت نشد",
    editWindowTite: "ویرایش",
    dataEntryPeriod: "بازه ورود اطلاعات",
    datePeriod: "محدوده تاریخ",
    datePeriodType_active: "جاری",
    datePeriodType_all: "همه",
};

userContractListStrs = {
    grid_LatestProgressPeriodCheckDate: "دوره جاری (منتهی به)",
    grid_LatestSavedProgressPeriodCheckDate: "آخرین دوره ذخیره شده (منتهی به)",
    grid_SaveProgress: "ثبت اطلاعات پیشرفت",
};


reportCartableStrs = {
    hotDFIs: "اقلام داغ",
    hotDFIs_description: "گونه های در بازه ثبت هستند که هنوز ثبت پیشرفت نشدند",
    allDFIs: "همه اقلام",
    allDFIs_description: "لیست تمامی اقلام کاری مرتبط با شما",
    wbs: "درختواره اقلام",
    wbs_description: "شکست کار کلیه اقلام کاری مرتبط با شما",
    finalizeAlert: "پس از نهایی کردن امکان ویرایش خوداظهاری نخواهد بود.آیا از انجام این کار اطمینان دارید؟",
    selfDeclareFilalized: 'خود اظهاری نهایی شد.'
};

////////////////////////////////////////////////////// Peridic Report Ends ///////////////////////////////////////////////////////

projectPlanCompareStrs = {
    planTitle: "عنوان برنامه",
    chartPlanColor: "رنگ نمودار برنامه",
    chartActualColor: "رنگ نمودار واقعی",
};

resourceUsageReportsStrs = {
    usageDate: 'تاریخ مصرف',
    relatedDFIRootPath: "مسیر ریشه فعالیت مربوطه",
    usageAmount: 'میزان مصرف',
    usageComments: 'شرح مصرف',
    creationTime: 'تاریخ ثبت',
};

resourceUsageHistogramReportsStrs = {
    chartTitle: 'مصرف منابع',
    weekly: 'هفتگی',
    monthly: 'ماهانه',
    pleaseSelectAResource: 'لطفا منبع را انتخاب کنید',
    fromCheckDate: 'شروع بازه',
    toCheckDate: 'اتمام بازه',
    usageAmount: 'میزان مصرف',
};

workResourceAssignStrs = {
    chartTitle: 'نمودار تخصیص منابع کاری',
    startDateShouldBeAfterEndDate: "تاریخ شروع میبایست پس از تاریخ پایان باشد",
};

resourceAssignStrs = {
    chartTitle: 'نمودار تخصیص منابع ',
};

projectsStr = {
    filters: "فیلترها",
    gridColumns: {
        ProjectNO: "کد",
        Name: "عنوان پروژه",
        ContractNO: "شماره قرارداد",
        ContractDate: "تاریخ قرارداد",
        ProgramTitle: "طرح",
        DefaultLocationTitle: "محل",
        ProjectStatusName: "وضعیت پروژه",
        ProjectTypeName: "نوع پروژه",
        Currency: "نوع ارز پروژه",
        ProjectDateType: "نوع تاریخ پروژه",
        PMFullName: "راهبر پروژه",
        PMUserName: "نام کاربری راهبر پروژه",
    }
};

wbsPatternsStr = {
    Name: "نام",
    Code: "کد",
    DefaultDuration: "طول پیش فرض",
    Weight: "وزن",
    Unit: "واحد",
};

projectVolumesStrs = {
    titleCaptionGrid: "عنوان کار انجام شده",
    volumn: "حجم کار انجام شده",
    unit: "واحد کار انجام شده"
};

advancedToolsStrs = {
    transferDataFromProject: "انتقال اطلاعات از پروژه",
    roles: "نقشها",
    projects: "پروژه ها",
    project: "پروژه",
    rolesAccesses: "دسترسی نقشها",
    addedAccess: "تعداد {0} دسترسی اضافه شد",
    noAccessAdded: "هیچ دسترسی اضاضه نشد",
    notFoundRoles: "نقش های یافت نشده",
    done: "انجام شد",
    copyFinished: "عملیات کپی انجام شد",
    addedWorkflows: "تعداد {0} چرخه کپی شد",
    addedWBS: "تعداد {0} شکست کار شامل {1} ردیف اطلاعات کپی شد",
    defaultProjectWBSSet: "شکست کار پیش فرض پروژه تنظیم شد",
    defaultProjectPlanSet: "برنامه پیش فرض  پروژه تنظیم شد",
    addedIssueCategory: "تعداد {0} دسته بندی اضافه شد",
    addedPlan: "تعداد {0} برنامه زمانبندی شامل {1} ردیف اطلاعات کپی شد",

    issueCategoryIsDuplicated: "دسته بندی: '{0}' تکراریست",
    noIssueCategoryAdded: "هیچ دسته بندی اضافه نشد",
    dataFormItemPlanProgressCalculationMethod: "روشهای محاسبه پیشرفت برنامه ای اقلام کاری پروژه",

    workflows: "چرخه ها",
    wBSs: "شکست کار ها",
    issueCategories: "دسته بندی مشکلات پروژه",
    riskCategories: "دسته بندی ریسکهای پروژه",

    dfiWorkflowsResettedAlert: "عملیات ریست چرخه ها انجام شد",
    dfiWorkflowsResetted: "تعداد {0} چرخه اقلام کاری ریست شد",
    noItemFoundToReset: "هیچ آیتم آماده ای برای ریست چرخه یافت نشد",

    dfiWorkflowsStartedAlert: "عملیات شروع چرخه ها انجام شد",
    dfiWorkflowsStarted: "تعداد {0} چرخه اقلام کاری شروع شد",
    noItemFoundToStart: "هیچ آیتم آماده ای برای شروع چرخه یافت نشد",

    userAddedToRole: "کاربر '{0}' به نقش '{1}' اضافه شد",
    noUserAddedToRoles: "تمامی کاربران در تمامی پروژه ها قبلا درج شده اند و یا این نقش در هیچ پروژه ای وجود ندارد.",

    addedDFIs: "تعداد {0} قلم کاری ایجاد شد",
    workflowIsNotDefinedInThisProject: "چرخه کاری '{0}' در این پروژه تعریف نشده است",
    dfippcmIsNotDefinedInThisProject: "روش محاسبه پیشرفت برنامه ای اقلام کاری '{0}' در این پروژه تعریف نشده است",
    thisProjectNotHaveDefaultPlan: "این پروژه برنامه پیش فرض ندارد.",
};

projectDisciplinesStrs = {
    newWindowTitle: 'دیسیپلین جدید',
    editWindowTitle: 'ویرایش دیسیپلین',
    canNotRemoveBecauseOFRelatedDFIs: "با توجه به ارتباط تعدادی قلم کاری با این آیتم، امکان حذف آن وجود ندارد",
    canNotRemoveBecauseOFRelatedPayments: "با توجه به ارتباط تعدادی پرداخت با این آیتم، امکان حذف آن وجود ندارد",
    grid_Name: "نام",
    grid_Description: "شرح",
    grid_ManagerUserLastName: "مسئول دیسیپلین",
    grid_RelatedDataformItemsCount: "اقلام کاری مرتبط",

};
////////////////////////////////////////////////////// Message Dispatch Module Starts /////////////////////////////////////////
sessionManagementStrs = {
    attachedDocuments: 'مستندات الحاقی',
    proceeding: 'صورتجلسه',
    userIsAssignedToAnotherSession: "شرکت کننده انتخاب شده {0} در زمان انتخاب شده در جلسه {1} حضور دارد ",
    roomIsAssignedToAnotherSession: "در اتاق انتخاب شده {0} در زمان انتخاب شده، جلسه {1} برقرار است ",
    sessionRoom: "اتاق جلسه",
    sessionType: "نوع جلسه",
    sessionTitle: "عنوان جلسه",
    sessionNo: "شماره جلسه",
    sessionAgendaNo: "شماره دستور کار جلسه",
    sessionTime: "زمان",
    sessionStart: "شروع",
    sessionEnd: "خاتمه",
    project: "پروژه",
    projects: "پروژه های مرتبط",
    userAttendees: "کاربران دعوت شده",
    outerAttendees: "مدعوین خارج سیستم",
    informedUsers: "مطلعین",
    organizerUser: "دبیر جلسه",
    sessionSubjectDefaultValue: "بدون موضوع",
    editProceedingRow: "ویرایش بند صورتجلسه",
    procTitle: "عنوان",
    procStatus: "وضعیت",
    procResponsibles: "مسئول",
    procDueDate: "سر رسید",
    procDescription: 'شرح',
    procChangeHistory: 'تاریخچه تغییرات',
    procPriority: 'اولویت',
    showTypes: {
        user: "کل جلسات مرتبط با من",
        users: "کاربران مرتبط",
        userCreated: "جلسات ایجاد شده بوسیله من",
        rooms: "اتاق جلسات سازمان",
        allOrganization: "تمام جلسات سازمان",
        allSystem: "تمام جلسات سیستم",
    },
    invitedsStatus: {
        invitedUserTitle: 'عنوان مدعو',
        notSystemUser: 'مدعو خارج سیستمی',
        sessionPresenceStatus: 'وضعیت حضور در جلسه'
    },
    reports: {
        totalProceedingsCount: 'تعداد کل وظایف',
        doneProceedingsCount: 'تعداد وظایف انجام شده',
        remainingProceedingsCount: 'تعداد وظایف باقیمانده',
    },
    agg_session_HasNoAccess: "شما به این بخش دسترسی ندارید. برای دسترسی به این بخش میبایست دارای دسترسی راهبر پروژه و یا مشاهده تجمیع اسناد جلسات در پروژه باشید ",
};
////////////////////////////////////////////////////// Message Dispatch Module Ends /////////////////////////////////////////
////////////////////////////////////////////////////// Message Dispatch Module Starts /////////////////////////////////////////

SmartAlert = {
    EventTypeName: "عنوان دسته",
    EventsCount: "تعداد رخداد",
    ExtensionName: "عنوان ماژول",
    EventTitle: "عنوان رخداد",
    EventName: "نام سیستمی",
    EventRegistersCount: "رخداد گردان",
    EventRegistersTitle: "عنوان رخداد گردان",
};

//////////////////////////////////////////////////////     Message Dispatch Module Ends /////////////////////////////////////////



///////////////////////////////////////////////////////    Engineering Module Starts /////////////////////////////////////////

engineeringStrs = {
    taskMasterDocumentNumber: "شماره سند کارفرما",
    contractorDocumentNumber: "شماره سند پیمانکار",
    reservedDocumentNumber: "شماره سند رزرو",
    supervisorDocumentNumber: "شماره سند ناظر",
    documentClass: "کلاس مدرک",
    documentType: "نوع مدرک",
    isReferableInCurrentState: "امکان ارجاع در این مرحله",
    isTransmitableInCurrentState: "امکان ارسال در این مرحله",
    someAreNonTransmitableMessage: "از مجموعه {0} مدرک انتخاب شده {1} مدرک در مرحله ای نیست که بتوان آنرا به مرسوله الصاق کرد",
    weight: "وزن",
    documentNo: "شماره مدرک",
    totalPlannedManHour: "نفر ساعت برنامه ریزی شده",
    //aeIDC
    ReferenceReturn: "بازگشت ارجاع",
    ReferenceReceiver: "گیرنده ارجاع",
    ReferenceEdit: "ویرایش ارجاع",
    ReferenceText: "متن ارجاع",
    ReferenceNew: "ثبت ارجاع جدید",
    documentReferType: "نوع ارجاع",
    TheDeadlineForReplies: "مهلت پاسخ",
    AbilityToDereference: "امکان ارجاع مجدد",

    RefererDiscipline: "از دیسیپلین",
    ReferedToDiscipline: "به دیسیپلین",


    //aeTransmital
    DocumentNoEmployer: "شماره مدرک کرفرما",
    DocumentNoContractor: "شماره مدرک پیمانکار",
    ViewItem: "مشخصات مرسوله",
    EngineeringDocumentsAttached: "مدارک مهندسی الصاق شده",
    PublicDocumentsAttached: "مدارک عمومی الصاق شده",
    TransmitalNumber: "شماره مرسوله",
    TransmitalDate: "تاریخ مرسوله",
    TransmitalFrom: "از",
    TransmitalTo: "به",
    ReferenceCode: "کد مرجع",
    Transmital_Subject: "عنوان",
    Transmital_Remarks: "توضیحات",
    DocumentsAttachedAutomaticallyGoNextStep: "مدارک الصاق شده به صورت خودکار به مرحله بعد بروند",
    DocumentAttach: "مدارک الصاق شده به مرسوله ",
    DocumentAttachable: "مدارک قابل الصاق به مرسوله",
    DocumentAttachCount: "تعداد مدارک الصاق شده",
    RecieveDate: "تاریخ دریافت",
    DocumentCount: "تعداد مدرک",
    AttachedDocumentStatus: "وضعیت مدرک هنگام الصاق",
    DocumentVersionAttached: "نسخه مدرک هنگام الصاق",
    ClientDocumentNumber: "شماره سند کارفرما",
    DocumentNumberContractor: "شماره سند پیمانکار",
    DocumentLevel: "سطح مدرک",
    EditNumber: "شماره ویرایش",
    TransmitalDocs_Grid_Position: "ردیف",
    TransmitalDocs_Grid_From: "از",
    TransmitalDocs_Grid_To: "به",
    TransmitalDocs_Grid_TimeRevNo: "ویرایش",
    TransmitalDocs_Grid_TimeState: "وضعیت",
    TransmitalDocs_Grid_PageCount: "تعداد صفحات",
    TransmitalDocs_Grid_DisciplineName: "دیسیپلین",
    TransmitalDocs_Grid_ActivityCode: "شناسه فعالیت",
    TransmitalDocs_Grid_WBSCode: "شناسه شکست",
    TransmitalDocs_Grid_WorkflowModelName: "نام چرخه",
    TransmitalDocs_Grid_CurrentState: "وضعیت کنونی",
    TransmitalDocs_Grid_ExtensionName: "نوع قلم کاری",
    TransmitalDocs_Grid_ProjectDataFormItemProgressPlanName: "برنامه پیشرفت",
    TransmitalDocs_Grid_PlanStartDate: "شروع برنامه ای",
    TransmitalDocs_Grid_PlanEndDate: "پایان برنامه ای",
    TransmitalDocs_Grid_ForecastEndDate: "پایان پیش بینی",
    TransmitalDocs_Grid_ActualStartDate: "شروع و",
    TransmitalDocs_Grid_ActualEndDate: "پایان و",
    TransmitalDocs_Grid_WeightOfWhole: " وزن از کل",
    TransmitalDocs_Grid_WeightOfActivity: "وزن از فعالیت",
    TransmitalDocs_Grid_PlanManHour: "نفرساعت برنامه ای",
    transmiltal_TransmitalDateIsMandatory: "ورود تاریخ مرسوله اجباریست",
    TransmitalDocs_Grid_DateType: 'نوع تاریخ',

    SheetCount: "تعداد صفحات",
    DocumentSize: "قطع مدرک",
    SupervisorDocNum: "شماره سند مشاور",
    DocumentType: "نوع مدرک",
    ReservedDocNumber: "شماره سند رزرو",
    DocumentClass: "کلاس مدرک",
    DocumentTitle: "عنوان مدرک",

    newRefer: "ارجاع جدید",
    editRefer: "ویرایش ارجاع",
    removeRefer: "حذف ارجاع",
    reRefer: "ارجاع مجدد",
    postbackRefer: "بازگشت ارجاع",
    referedDocumentDetails: "مشخصات سند در زمان ارجاع",
    ViewReferenceItem: "مشخصات ارجاع",
    Reference: "ارجاعات",
    Items: "مرسولات",
    Comments: "نظرات",
    DueDate: "مهلت پاسخ",
    ReRefered: "ارجاع مجدد",
    RefererLastName: "ارجاع دهنده",
    ReferedToLastName: "ارجاع شده به",
    DescriptionReferenceTime: "توضیحات زمان ارجاع",
    PlanStartDate: "تاریخ ارجاع",
    DeadlineFoReturn: "مهلت بازگشت",
    TimeBack: "زمان بازگشت",
    DescriptionTimeBack: "توضیحات زمان بازگشت",
    TextCommentReferral: "متن  کامنت ارجاع دهنده",
    PostBackComments: "جوابیه بازگشت",
    EditItem: "ویرایش مرسوله",
    NewItem: "ثبت مرسوله جدید",
    NewTransmital: "ثبت مرسوله جدید",
    Project: "پروژه",

    //IDCs DocumentRefers
    waitingTime: "زمان انتظار",
    engiHumanResourceReport: "گزارش نیروی انسانی مهندسی",
    waitingTimeByDisciplinTitle: "زمان به تفکیک دیسیپلین",
    waitingTimeByUserTitle: "زمان به تفکیک نیروی انسانی",

    idcMatrix: "ماتریس ارجاع",

};
w_engStepsDurationDenotativeStrs = {
    titlePrefix: "آمار تفکیکی میانگین زمان صرف شده در هر استپ بر اساس",
    documentClass: "کلاس مدرک",
    documentType: "نوع مدرک",
    documentDiscipline: "دیسیپلین مدرک",

    settings_displayBy: "نمایش بر اساس",
};


w_engDataformItemsStateCountDenotativeStrs = {
    titlePrefix: "آمار تفکیکی تعداد مدارک مهندسی در وضعیتهای مختلف بر اساس",
    documentClass: "کلاس مدرک",
    documentType: "نوع مدرک",
    documentDiscipline: "دیسیپلین مدرک",
    settings_displayBy: "نمایش بر اساس",
};

w_engDataformItemsStateCount = {
    title_count: "تعداد",
};

///////////////////////////////////////////////////////    Engineering Module Ends /////////////////////////////////////////
//////////////////////////////////////////////////////     Procurement Module Starts /////////////////////////////////////////

procurementStrs =
{
    YouHaveNoPermissionToViewThisSection: "شما دسترسی لازم برای مشاهده این بخش را ندارید",
    ProcurementType: "نوع تدارک",
    MRNumber: "شماره MR",
    MaterialType: "نوع تدارک",
    MRMTOIssueDate: "MR/MTO تاریخ صدور",
    MRMTOPublishDate: "MR/MTO تاریخ انتشار",
    TotalQuantity: "مقدار کل",
    QuantityUnit: "واحد مقدار",
    Materials: "جزئیات تدارک",
    VendorSelection: "انتخاب تامین کننده",
    Milestons: "مایلستونها",
    Vendors: "تامین کنندگان",
    VendorSelectionFinalization: "انتخاب نهایی تامین کننده",
    VendorSelectionMethod: "روش انتخاب تامین کننده",
    InqueryRFQDate: "تاریخ RFQ/استعلام",
    TechnicalClarificationDate: "تاریخ تصریح فنی",
    InqueryBIDCloseDate: "تاریخ بسته شدن مناقصه/استعلام",
    TBEIssueDate: "TBE تاریخ صدور",
    TBEApproveDate: "TBE تاریخ تایید",
    CBEIssueDate: "CBE تاریخ صدور",
    CBEApproveDate: "CBE تاریخ تایید",
    FinalVendorSelectionMR1Date: "(MR1) تاریخ انتخاب تامین کننده نهایی",
    KOMDate: "تاریخ جلسه شروع (KOM)",
    MRRevisionsPO: "نسخ MR و دستور خرید",
    POPlacementDate: "PO تاریخ گمارش",
    POEffectDate: "PO  تاریخ موثر",
    PONumber: "PO شماره",
    ManufacturingStartDate: "تاریخ شروع تولید",
    ManufacturingFinishDate: "تاریخ پایان تولید",
    FirstInspectionDate: "تاریخ اولین بازرسی",
    InspectorThirdParty: "بازرس(Third Party)",
    FinalInspectionDate: "تاریخ آخرین بازرسی",
    Manufacturing: "تولید",
    Inspection: "بازرسی",
    Insurance: "بیمه",
    TransporFinalization: "حمل و نقل و نهایی سازی",
    Financial: "مالی",
    ShipmentTerm: "Shipment Term",
    BLNumber: "شماره بارنامه (BL)",
    BillofLoadningDate: "تاریخ بارنامه",
    DepartureDate: "تاریخ حرکت",
    CustomsArrivalDate: "تاریخ رسیدن به گمرک",
    CustomsClearanceDate: "تاریخ ترخیص",
    SiteTransportatinDate: "تاریخ حمل به سایت",
    ROSDate: "تاریخ رسیدن به سایت (ROS)",
    FinalDocumentationDate: "تاریخ مستند سازی نهایی",
    TotalAmountPlan: "مبلغ کل پیش بینی",
    TotalAmountPlanCurrencyID: "نوع ارز",
    TotalAmountActual: "مبلغ کل واقعی",
    TotalAmountPlanActualID: "نوع ارز",
    Guaranties: "تضامین",
    Payment: "پرداخت",
    LCs: "اعتبارات اسنادی",
    //Material Specification Plan
    MaterialSpecification: "جزئیات تدارک",
    MaterialSpecificationPlan: "برنامه جزئیات تدارک",
    MaterialSpecificationActual: "جزئیات تدارک نهایی",
    MaterialTypeCode: "کد دسته بندی تدارک",
    Description: "شرح",
    Quantity: "مقدار",
    QuantityUnitAbbriviation: "واحد",
    UnitPrice: "مبلغ واحد",
    PriceUnitAbbriviation: "واحد",
    UnitWeight: "وزن واحد",
    DimWidth: "عرض",
    DimensionUnitAbbriviation: "واحد",
    DimLength: "طول",
    ID: "ID",
    Width: "عرض",
    Length: "طول",
    Height: "ارتفاع",
    RefrencePlanItem: "آیتم برنامه",
    ////Vendor
    FinalSelectedVendor: "تامین کننده انتخاب شده نهایی(MR (1))",
    VendorName: "نام تامین کننده",
    Origin: "کشور مبدا",
    RFQActualDate: "RFQ تاریخ واقعی",
    RFQResponseDate: "RFQ تاریخ پاسخ به",
    TechnicalStatus: "وضعیت فنی",
    DeliveryDuration: "مدت زمان تحویل",
    TotalQuotedPrice: "مبلغ کل پیشنهادی",
    RFQResponseStatus: "وضعیت پاسخ به RFQ",
    AddNewVendor: "تامین کننده بالقوه جدید",
    EditVendor: "ویرایش تامین کننده بالقوه",
    ///Revision
    MRRevisions: "نسخ MR",
    RevisionNumber: "شماره نسخه",
    RevisionDate: "تاریخ نسخه",

    ///Manufacturing
    ManufacturingDuration: "مدت زمان تخمین تولید",
    ManufacturingProgressAssessmentPeriods: "دوره های پایش وضعیت پیشرفت",
    ManufacturingAddWindow: "افزودن ارزیابی پیشرفت تولید",
    ManufacturingEditWindow: "ویرایش ارزیابی پیشرفت تولید",
    Progress: "پیشرفت تولید",
    ProgressDate: "تاریخ ارزیابی پیشرفت",
    ProgressPercent: "میزان پیشرفت",
    //Inspection 
    InspectionDetails: "جزئیات بازرسی",
    InspectionStakeholderName: "تامین کننده",
    InspectionTitle: "عنوان",
    InspectionDate: "تاریخ",
    InspectionDateA: "تاریخ واقعی بازرسی",
    InspectionDateP: "تاریخ برنامه بازرسی",
    InspectionDateF: "تاریخ پیش بینی بازرسی",
    InspectionStatus: "وضعیت",
    EditInspection: "ویرایش بازرسی",
    AddInspection: "بازرسی جدید",
    //Insurance
    InsuranceStakeholder: "تامین کننده",
    InsuranceName: "نام",
    InsuranceDate: "تاریخ",
    InsuranceAmount: "مبلغ",
    InsuranceCurrencyUnit: "واحد",
    InsuranceRate: "نرخ",
    InsuranceValidAfterArrival: "اعتبار پس از رسیدن",
    InsuranceValidAfterArrivalDuration: "مدت اعتبار پس از رسیدن",
    AddNewInsurance: "ثبت بیمه جدید",
    EditInsurance: "ویرایش بیمه",
    //Financial&Guranty
    FinancialGurantyCurrencyName: "واحد",
    FinancialGurantyAmount: "مبلغ",
    FinancialGurantyDate: "تاریخ بیمه",
    FinancialGurantyExpiretionDate: "تاریخ انقضاء",
    FinancialGurantyTypeID: "نوع بیمه مالی",
    FinancialGurantyCurrencyUnitID: "واحد",
    FinancialGurantyRefrenceNumber: "کد مرجع",
    FinancialGurantyNotes: "توضیحات",
    FinancialLCNumber: "شماره LC ",
    FinancialLCIssueDate: "تاریخ صدور",
    FinancialLCCurrencyName: "واحد",
    FinancialLCAmount: "مبلغ",
    FinancialLCExpirationDate: "تاریخ انقضاء",
    FinancialLCConditions: "شرایط",
    FinancialLCNotes: "شرح",
    AddNewLC: "ثبت اعتبار اسنادی جدید",
    EditLC: "ویرایش اعتبار اسنادی",
    AddNewFinancialGuranty: "ویرایش تضمین مالی",
    EditFinancialGuranty: "ویرایش تضمین مالی",

    ///window caption
    MaterialSpecificationPlanNewItem: "Material Specification Plan NewItem",
    MaterialSpecificationPlanEditItem: "Material Specification Plan EditItem",
    AddNewRevision: "نسحه جدید",
    RevisionDetails: "جزئیات نسخه",
    totalPlannedManHour: "نفر ساعت برنامه ریزی شده",




    PlanAbbr: " ب",
    ActualAbbr: " و",
    ForecastAbbr: " پ",
    DocumentTitle: "عنوان قلم خرید",
};

//////////////////////////////////////////////////////     Procurement Module Ends /////////////////////////////////////////

/////////////////////////////////////////////////////      Chat Module Starts      /////////////////////////////////////////
chatStrs = {
    noFileSelected: "فایلی برای ارسال انتخاب نشده است",
    noOlderMessage: "پیام قدیمی تری وجود ندارد",
    imageLoadingProblem: "مشکلی در بارگذاری تصویر",
    you: "شما",
    loading: "در حال بارگذاری",
    userName: "نام کاربری",
    firstName: "نام",
    lastName: "نام خانوادگی",
    enterAtLeastThreeWordsForSearch: "برای جستجو حداقل 3 حرف وارد کنید",
    newMessageFrom: "پیام جدید از",
    otherMessages: "مورد دیگر",
    clickSend: "برای ارسال پیام کلیک کنید",
    chatInfo: "اطلاعات تماس",
    chatwith: "گفتگو با ",
    videoCallto: "تماس تصویری با ",
    voiceCallto: "تماس صوتی با "
};

chatGroupStrs = {
    GroupName: "نام گروه",
    participants: "اعضاء",
    AddParticipant: "افزودن عضو",
    MemberSearch: "جستجوی کاربر",
    ExitGroup: "ترک گروه",
    DeleteGroup: "حذف گروه",
};

/////////////////////////////////////////////////////      Chat Module Ends      /////////////////////////////////////////



/////////////////////////////////////////////////////      MDR Starts      /////////////////////////////////////////
mdrImportStrs = {
    pleaseSelectRequiredField: "لطفا موارد الزامی انتخاب شوند",
    pleaseSelectFile: "لطفا فایل را انتخاب نمایید",
    fieldContent: "محتوای فیلد",
    fieldNameInExcel: "نام فیلد در فایل اكسل",
    selectColumns: "انتخاب ستون ها",
    firstStepInfo1: " توجه داشته باشید كه فایل MDR به لیست مدارك افزوده میشوند و در صورت تكراری بودن مدارك موجود به روز نخواهند شد",
    selectiveProjectPlan: "برنامه زمانبندی انتخابی",
    selectExcelFile: "فایل Excel مورد نظر را با توجه به بخش زیر انتخاب نمایید",
    firstStepInfo2: "توجه : <br />" +
        "<ul style='list-style-type: none'>" +
        "    <li>1- در سطر اول فایل اکسل ستونها بهتر است مشابه شكل زیر با نامهای (No,WBSNo,Weight,DocumentNo,DocumentTitle,ActivityCode,Descipline,DocumentType,DocumentClass,TotalPlannedManHour)" +
        "    نامگذاری شوند<br />" +
        "      <img src='/images/MDRExcelSample.png' />" +
        "</li>" +
        "    <li>2- تاریخ های شروع و پایان برنامه ریزی باید بصورت میلادی و در قالب yyyy/mm/dd باشد.</li>" +
        //"    <li>&nbsp;3- در صورت میلادی بودن تاریخ های پروژه، تاریخها به همگی به صورت میلادی و با فرمت&nbsp;" +
        //"yyyy/mm/dd باشند</li>" +
        "    <li>3- اطیمنان حاصل كنید كه در نام شیت حاوی اطلاعات مدارك كاراكترهایی غیر از حرف و عدد" +
        "    وارد نشده باشد و نام شیت به صورت انگلیسی باشد.</li>" +
        "    <li>4- مجموع وزن مدارک می بایست برابر با وزن فاز مهندسی باشد</li>" +
        "</ul>" +
        "</li>" +
        "</ul>",
    uploadMDRFile: "بارگذاری فایل اكسل MDR",
    selectSheet: "انتخاب Sheet",
    pleaseSelectSheet: "لطفا شیت اكسل حاوی اطلاعات مدارك را انتخاب كنید",
    finalCheck: "بررسی نهایی",
    lastStep: "مرحله آخر",
    newDisciplineInfo: "لیست دیسلینهایی كه در فایل MDR وارد شده وجود دارند اما در لیست دیسلینهای پروژه وجود ندارد",
    createNewDiscipline: "دیسیپلین هایی كه موجود نیستند را در پروژه ایجاد كن",
    notCreateNewDiscipline: "دیسیپلین هایی كه در پروژه موجود نیستند را برای مدارك مقدار خالی قرار بده",
    newClassInfo: "لیست كلاسهایی كه در فایل MDR وارد شده وجود دارند اما در لیست كلاسهای پروژه وجود ندارد",
    createNewClass: "كلاسهایی كه موجود نیستند را در پروژه ایجاد كن",
    notCreateNewClass: "كلاسهایی كه در پروژه موجود نیستند را برای مدارك مقدار خالی قرار بده",
    dataEntry: "ورود اطلاعات",
    allDisciplineExists: "تمامی دیسیپلینها در پروژه موجودند",
    allClassExists: "تمامی کلاس ها در پروژه موجودند",
    dataEntrySucceed: "ورود اطلاعات با موفقیت انجام شد.",
    viewMDRPage: "مشاهده صفحه MDR",
    connectEDFIToWbs: "جهت اتصال مدارک مهندسی به ساختار شکست کار، از صفحه {0} استفاده نمایید",


};
/////////////////////////////////////////////////////      MDR Ends      /////////////////////////////////////////


/////////////////////////////////////////////////////      ProcurementItem Starts      /////////////////////////////////////////
piImportStrs = {
    pleaseSelectRequiredField: "لطفا موارد الزامی انتخاب شوند",
    pleaseSelectFile: "لطفا فایل را انتخاب نمایید",
    fieldContent: "محتوای فیلد",
    fieldNameInExcel: "نام فیلد در فایل اكسل",
    selectColumns: "انتخاب ستون ها",
    firstStepInfo1: " توجه داشته باشید كه فایل PI به لیست مدارك افزوده میشوند و در صورت تكراری بودن مدارك موجود به روز نخواهند شد",
    selectiveProjectPlan: "برنامه زمانبندی انتخابی",
    selectExcelFile: "فایل Excel مورد نظر را با توجه به بخش زیر انتخاب نمایید",
    firstStepInfo2: "توجه : <br />" +
        "<ul style='list-style-type: none'>" +
        "    <li>1- در سطر اول فایل اکسل ستونها بهتر است با نامهای (No,WBSNo,Weight,Title,ActivityCode,Descipline,ProcurementType)" +
        "    نامگذاری شوند<br />" +
        "</li>" +
        "    <li>2-تاریخ های شروع و پایان برنامه ریزی باید بصورت میلادی و در قالب yyyy/mm/dd باشد.</li>" +
        //"    <li>&nbsp;3- در صورت میلادی بودن تاریخ های پروژه، تاریخها به همگی به صورت میلادی و با فرمت&nbsp;" +
        //"yyyy/mm/dd باشند</li>" +
        "    <li>3- اطیمنان حاصل كنید كه در نام شیت حاوی اطلاعات مدارك كاراكترهایی غیر از حرف و عدد" +
        "    وارد نشده باشد و نام شیت به صورت انگلیسی باشد.</li>" +
        "    <li>4- مجموع وزن مدارک می بایست برابر با وزن فاز خرید باشد</li>" +
        "</ul>" +
        "</li>" +
        "</ul>",
    uploadProcurementItemFile: "بارگذاری فایل اكسل PI",
    selectSheet: "انتخاب Sheet",
    pleaseSelectSheet: "لطفا شیت اكسل حاوی اطلاعات مدارك را انتخاب كنید",
    finalCheck: "بررسی نهایی",
    lastStep: "مرحله آخر",
    newDisciplineInfo: "لیست دیسلینهایی كه در فایل PI وارد شده وجود دارند اما در لیست دیسلینهای پروژه وجود ندارد",
    createNewDiscipline: "دیسیپلین هایی كه موجود نیستند را در پروژه ایجاد كن",
    notCreateNewDiscipline: "دیسیپلین هایی كه در پروژه موجود نیستند را برای مدارك مقدار خالی قرار بده",
    newProcurementTypeInfo: "لیست نوع تدارک ها كه در فایل PI وارد شده وجود دارند اما در لیست نوع تدارک های پروژه وجود ندارد",
    createNewProcurementType: "نوع تدارک هایی كه موجود نیستند را در پروژه ایجاد كن",
    notCreateNewProcurementType: "نوع تدارک هایی كه در پروژه موجود نیستند را برای مدارك مقدار خالی قرار بده",
    dataEntry: "ورود اطلاعات",
    allDisciplineExists: "تمامی دیسیپلینها در پروژه موجودند",
    allProcurementTypeExists: "تمامی نوع تدارک ها در پروژه موجودند",
    dataEntrySucceed: "ورود اطلاعات با موفقیت انجام شد.",
    viewProcurementItemPage: "مشاهده صفحه PI",
    connectPDFIToWbs: "جهت اتصال اقلام خرید به ساختار شکست کار، از صفحه {0} استفاده نمایید",
};
/////////////////////////////////////////////////////      ProcurementItem Ends      /////////////////////////////////////////


/////////////////////////////////////////////////////      Import WBS Start      /////////////////////////////////////////

wbsImportStrs =
{
    createWbs: "ایجاد ساختار شکست کار جدید",
    updateWbs: "تغییر ساختار شکست کار موجود",
    uploadWBSFile: "بارگذاری فایل پروژه",
    selectFile: "فایل مورد نظر را انتخاب فرمایید",
    pleaseSelectFile: "لطفا فایل را انتخاب نمایید",
    firstStepInfo2: "<span style='font-weight:bold;color:red;'> توجه  : </span><br />" +
        "<ul style='list-style-type: none'>" +
        "   <li><span style='font-weight:bold;'> 1-</span>فرمتهاي قابل قبول:</li>" +
        "   <li>" +
        "       <ul  style='list-style-type: none'>" +
        "           <li style='margin-right:10px;'>نرم افزار  Microsoft Project نسخه 2007 و یا بالاتر فرمت های  MPP, MPT, MPX, MSPDI </li>" +
        "           <li style='margin-right:10px;'>نرم افزار  Primavera P6  فرمت  XER</li>" +
        "           <li style='margin-right:10px;'>نرم افزار  Primavera P3 فرمت PRX</li>" +
        "           <li style='margin-right:10px;'>نرم افزار  Plannerفرمت XML </li>" +
        "           <li style='margin-right:10px;'>نرم افزار SureTrak  فرمت  STX</li>" +
        "           <li style='margin-right:10px;'>نرم افزار Asta Powerproject فرمت های PP, MDB</li>" +
        "           <li style='margin-right:10px;'>نرم افزار  Phoenix Project Manager فرمت PPX</li>" +
        "           <li style='margin-right:10px;'>نرم افزار GanttProject فرمت GAN </li>" +
        "       </ul>" +
        "   </li>" +
        "   <li><span style='font-weight:bold;'> 2-</span> بهتر است ستون ActivityCode را جهت ارتباط خودکار مدارک و فعالیتها در فایل MSP در فیلد text1 مقدار دهید. با این روش میتوانید در بخش ایجاد ارتباط خودکار اقدام به ارتباط خودکار اقلام کاری با سطوح WBS کنید." +
        "   </li>" +
        "   <li><span style='font-weight:bold;'> 3-</span>در صورت نیاز مقدار وزن هر سطح را در فیلد Number1 فایل MSP و با مقدار دهی از 100 وارد کنید." +
        "   </li>" +
        "   <li><span style='font-weight:bold;'> 4-</span>در صورت استفاده از نرم افزار پریماورا وزن فعالیت ها را در فیلد Budgeted Labor Units ذخیره کنید." +
        "   </li>" +
        "   <li><span style='font-weight:bold;'> 5-</span>توجه داشته باشید برای سهولت در امر به روز رسانی زمانبندی و شکست کار دقت شود که فایل ورودی دارای کدهای فعالیت و WBS تکراری نباشد." +
        "   </li>" +
        "   <li><span style='font-weight:bold;'> 6-</span> نام فعالیت در WBS بیشتر از 500 کاراکتر نباشد." +
        "	</li>" +
        "</ul>",
    importedFileHasError: "فایل انتخاب شده دارای مشکل می باشد",
    fileImportedSuccessfully: "فایل انتخاب شده با موفقیت بارگذاری شد",
    newWBSName: "نام شکست جدید",
    newPlanName: "نام برنامه زمانبندی جدید",
    createRelateBetweenDFIandWBS: "ایجاد خودکار ارتباط بین سطوح WBS و اقلام کاری و همچنین ایجاد زمانبندی آنها",
    basedOnRootPath: "بر اساس مسیر ریشه",
    basedOnWBSCodeOfDFI: "بر اساس شناسه WBS قلم کاری",
    basedOnActivityCodeOfDFI: "بر اساس شناسه فعالیت قلم کاری",
    basedOnWBSCode: "بر اساس شناسه WBS",
    basedOnActivityName: "بر اساس نام فعالیت",
    basedOnActivityCode: "بر اساس شناسه فعالیت",
    basedOnGUID: "بر اساس شناسه یکه WBS",
    updateResource: "به روز رسانی منابع",
    updateCalendar: "به روز رسانی تقویم های پروژه و تنظیم آن به عنوان پیش فرض",
    updateCBS: "ایجاد و بروزرسانی CBS پروژه بر اساس ستون Cost",
    pleaseFillInTheRequiredValues: "لطفا موارد الزامی را تکمیل نمایید",
    thirdStepInfo1: " ساختار شکست کار به شكل زیر شناسایی شده. در صورت تایید، این شكست به پروژه افزوده میشود.",
    mspImportSettings: "تنظیمات ورود",
    importSuccessInfo1: "ورود از فایل با موفقیت انجام شد.",
    wbsErrorList: "در زیر لیست شكستهایی كه در حین ورود با مشكل مواجه شدند را مشاهده میكنید.",
    importSuccessInfo2: "و شکست کار جدید به عنوان پیش فرض تنظیم شد.",
    workspacePage: "جهت دسترسی به فضای کار کلیک کنید.",
    howRelatePlanWithWbs: "نحوه اتصال زمانبندی جدید با ساختار شکست کار انتخاب شده",
    numOfErrors: "تعداد مشکلات",
    probabilityOfSuccess: "احتمال موفقیت",
    numOfItemsCanCover: "تعداد آیتمهای قابل پوشش"


};


/////////////////////////////////////////////////////      Import WBS End      /////////////////////////////////////////


/////////////////////////////////////////////////////      Currency Start      /////////////////////////////////////////

currencyStrs =
{
    conversionCurrency: "ارز تبدیلی",
    baseCurrency: "ارز پایه",
    exchangeRate: "نرخ تبدیل",
    effectiveDate: "تاریخ تاثیر",

};



/////////////////////////////////////////////////////      Currency End      /////////////////////////////////////////


/////////////////////////////////////////////////////      BI Starts      /////////////////////////////////////////

biStrs = {
    configur_messages: {
        measures: "[اندازه ها را اینجا رها کنید]",
        columns: "[ابعاد ستون را اینجا رها کنید]",
        rows: "[ابعاد سطر را اینجا رها کنید]",
        slices: '[ابعاد برش دهنده را اینجا رها کنید]',
        measuresLabel: 'اندازه ها',
        columnsLabel: 'ستونها',
        slicersLabel: 'برشها',
        rowsLabel: 'سطرها',
        fieldsLabel: 'فیلدها'
    },
    filters: "فیلترها",
    dropFiltersHere: 'فیلترها را اینجا رها کنید',
    hierarchy_Measures: "اندازه ها",
    hierarchy_KPIs: "KPI ها",
    reportShouldHaveAtLeastOneMeasure: "گزارش میبایست دارای حداقل 1 اندازه باشد",
    updateReoprtWindowTitle: "بروزرسانی گزارش",
    addReoprtWindowTitle: "ذخیره گزارش",
    specifyANameForReport: "لطفا عنوان گزارش را مشخص کنید",
    selectCube: "انتخاب مکعب",
    autoSliceSelect_assign: "تخصیص",
    autoSliceSelect_title: "عنوان",
    autoSliceSelect_selectAll: 'انتخاب همه',
};

/////////////////////////////////////////////////////      BI Ends      /////////////////////////////////////////

/////////////////////////////////////////////////////      Guarantee Starts      /////////////////////////////////////////

guaranteeStrs = {
    serialNO: "شماره سریال",
    amount: 'مبلغ',
    issuanceDate: "تاریخ صدور",
    dueDate: "تاریخ سر رسید",
    duration: "اعتبار",
    toggleIsRealPerson: "شخص ضامن",
    toggleName: "نام ضامن",
    beneficiary: "ضمانت خواه",
    toggleResidence: "اقامتگاه ضامن",
    bank: "بانک",
    bankBranch: "شعبه بانک",
    deliveryPlace: 'محل تحویل ضمانتنامه',
    transferee: 'تحویل گیرنده',
    guaranteeType: 'نوع ضمانتنامه',
    guaranteeFormat: 'فرمت ضمانتنامه',
    contractNo: 'شماره قرارداد',
    amountCurrency: 'ارز',
    toggleLegalEntityTypeName: 'ماهیت شخص ضامن',
    juridicallegal: "حقوقی",
    naturallegal: "حقیقی",
    other: 'سایر',
    specification: 'مشخصات',
    relatedDocs: 'اسناد مرتبط'
};



/////////////////////////////////////////////////////      Guarantee Ends      /////////////////////////////////////////



/////////////////////////////////////////////////////      Agile Starts      /////////////////////////////////////////
agileCommon =
{
};
scrumBoard = {
    items: 'مورد',
    point: 'امتیاز',
};
/////////////////////////////////////////////////////      Agile Ends      /////////////////////////////////////////

/////////////////////////////////////////////////////      Export Project Starts     /////////////////////////////////////////

exportProjectStrs = {
    pleaseSelectPlanandWBS: "لطفا برنامه زمانبندی و شکست کار انتخاب شود",
    pleaseSelectFileFormat: "لطفا فرمت فایل مشخص شود",
    useDefaultPlanandWBS: "استفاده از شکست کار، تقویم و برنامه زمانبندی پیشفرض پروژه",
    selectPlanandWBS: "تعیین شکست، زمانبندی و تقویم",
    wbsToUse: "شکست مورد استفاده",
    planToUse: "برنامه زمانبندی مورد استفاده",
    fileFormat: "فرمت فایل",
    downloadFile: "دریافت فایل"


};



/////////////////////////////////////////////////////      Export Project Ends      /////////////////////////////////////////

/////////////////////////////////////////////////////      Cost Center Starts     /////////////////////////////////////////

costCenterStrs = {
    cannotDeleteHasPayment: "به دلیل وجود پرداخت، قادر به حذف مرکز هزینه نیستید",
    cannotDeleteNotFound: "مرکز هزینه یافت نشد",
    cannotDeleteHasChild: "به دلیل وجود زیر مجموعه، قادر به حذف مرکز هزینه نیستید"


};



/////////////////////////////////////////////////////      Cost Center Ends      /////////////////////////////////////////

/////////////////////////////////////////////////////      Import Calendar Starts     /////////////////////////////////////////

importCalendarStr =
{
    selectCalendarFile: "فایل تقویم مورد نظر را انتخاب نمایید",
    uploadCalendarFile: "آپلود فایل تقویم",
    pleaseSelectCalendarFile: "لطفا فایل تقویم انتخاب شود",
    createCalendar: "ایجاد تقویم جدید",
    updateCalendar: "ویرایش تقویم موجود",
    name: "عنوان",
    description: "توضیح",
    selectCalendar: "انتخاب تقویم",
    updateWorkingTime: "بروزرسانی زمان کاری روزهای هفته",
    updateException: "بروزرسانی استثناها",
    removePreException: "حذف استثناهای قبلی",
    addToPreException: "اضافه کردن به استثناهای قبلی",
    dataEntrySucceed: "ورود تقویم با موفقیت انجام شد",
    dataEntryFailed: "در عملیات ورود تقویم اشکال پیش آمده است",
    finalStep: "مرحله آخر",
    importSettings: "تنظیمات",
    generalInfo: "مشخصات کلی",
    weekDays: "روزهای هفته",
    exceptions: "استثناها",
    uploadFileFailed: "در بارگذاری و پردازش فایل اشکال پیش آمده است",
    pleaseInsertName: "لطفا عنوان تقویم وارد شود",
    pleaseSelectCalendar: "لطفا تقویم پروژه جهت تغییر انتخاب شود",
    selectOneItemForEdit: "لطفا یک مورد برای ویرایش انتخاب شود",
    importCalendarFileTitle: "ورود تقویم از فایل",
}


/////////////////////////////////////////////////////      Import Calendar Ends     /////////////////////////////////////////
/////////////////////////////////////////////////////      Labelling Starts     ///////////////////////////////////////
labellingResources = {
    Title: "عنوان",
    ProjectName: "نام پروژه",
    Color: "رنگ",
    LabelTypeName: "نوع برچسب",
    StakeholderName: "ذینفع متبوع",
    newSave: "ایجاد برچسب جدید",
    editInfo: "ویرایش بر چسب",
    alertDelete: "آیا می خواهید برچسب انتخاب شده را حذف نمایید؟",
    alertSuccessDelete: "برچسب انتخاب شده با موفقیت حذف گردید",
    labelnotfound: "خطا در یافتن اطلاعات",
    labelHasRelatedItem: "برچسب انتخاب شده استفاده شده است. امکان حذف آن وجود ندارد",
};

/////////////////////////////////////////////////////      Labelling Ends     /////////////////////////////////////////
