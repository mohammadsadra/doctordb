﻿chartFonts = {
    generalFont: "IRANSansFaNum",
    titleFont: "bold 16px IRANSansFaNum",
};

worlflowDesignerStyleParams = {
    branchState: {
        font: "8pt IRANSansFaNum,tahoma, Arial, sans-serif",
        stork: 'whitesmoke',
    },
    documentApproval: {
        font: "9pt IRANSansFaNum,tahoma, Arial, sans-serif",
		stroke: 'whitesmoke',
    },
    dynamicParallelAcceptance: {
        font: "10pt IRANSansFaNum,tahoma",
        stroke: 'whitesmoke',
    },
    parallelAcceptanceStateSettings:
        {
            font: "10pt IRANSansFaNum,tahoma",
            stroke: 'whitesmoke',
        },
    finish: {
        font: "14px IRANSansFaNum,tahoma",
        stroke: 'whitesmoke',
    },
    generalDataFormItemStateSettings:
    {
        font: "10pt IRANSansFaNum,tahoma, Arial, sans-serif",
		stroke: "whitesmoke"
    },
    invoiceApprovalStateSettings:
    {
        font: "11pt IRANSansFaNum,tahoma, Arial, sans-serif",
        stroke: "whitesmoke"
    },
    multiUserChoiseStateSetting:
        {
            font: "9pt IRANSansFaNum,tahoma, Arial, sans-serif",
            stroke: "whitesmoke"
        }
    ,
    procurementItemStateSettings: {
        font: "10pt IRANSansFaNum,tahoma, Arial, sans-serif",
		stroke: "whitesmoke"
    },
    simpleAcceptRejectStateSettings: {
        font: "11pt IRANSansFaNum, Arial, sans-serif",
    },
    startManager: {
        font: "11pt IRANSansFaNum, Arial, sans-serif",
        stroke: "whitesmoke"
    },
    uTurnStateSettings: {
        font: "9pt IRANSansFaNum,tahoma, Arial, sans-serif",
        stroke: "whitesmoke"
    }
};