namespace SBDomain.Model.tmp
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class VW_Doctor
    {
        [Key]
        [Column(Order = 0)]
        public byte Id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(10)]
        public string Name { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(10)]
        public string FName { get; set; }

        [Key]
        [Column(Order = 3)]
        public byte SectionId { get; set; }

        [Key]
        [Column(Order = 4, TypeName = "date")]
        public DateTime BirthDate { get; set; }

        [Key]
        [Column(Order = 5)]
        [StringLength(15)]
        public string Expr2 { get; set; }
    }
}
