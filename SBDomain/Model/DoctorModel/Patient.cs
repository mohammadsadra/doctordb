namespace SBDomain.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Patient")]
    public partial class Patient
    {
        [Required]
        [StringLength(10)]
        public string Name { get; set; }

        [Required]
        [StringLength(10)]
        public string FName { get; set; }

        public byte Id { get; set; }

        public byte DoctorId { get; set; }

        [Column(TypeName = "date")]
        public DateTime InDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime OutDate { get; set; }

        [Required]
        [StringLength(10)]
        public string DiseaseType { get; set; }

        public byte PhoneNumber { get; set; }

        public byte NationalId { get; set; }

        [Required]
        [StringLength(50)]
        public string Address { get; set; }

        [Column(TypeName = "date")]
        public DateTime BirthDate { get; set; }

        public byte Weight { get; set; }

        public byte Height { get; set; }

        public virtual Doctor Doctor { get; set; }
    }
}
