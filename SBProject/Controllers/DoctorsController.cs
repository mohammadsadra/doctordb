﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using SBDomain.Model.DoctorModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SBProject.Controllers
{
    public class DoctorsController : ApiController
    {
        public DataSourceResult GetDoctors([System.Web.Http.ModelBinding.ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            using ( Model1 doctorDB = new Model1())
            {
                //var items = doctorDB.VW_Doctor.ToList();
                var items = doctorDB.Doctors.Select(e => new { id = e.Id, name = e.Name, fName = e.FName, sectionName = e.Section.Name }).ToList();
                return items.ToDataSourceResult(request);
            }
        }
    }
}
